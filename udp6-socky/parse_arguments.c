#include "parse_arguments.h"

/**
 * PRIVATE:
 *
 * Argumentos del programa
 *
 */
#define DEFAULT_OPT_RECV   false
#define DEFAULT_OPT_FROM   "::0"
#define DEFAULT_OPT_SPORT  8000
#define DEFAULT_OPT_DST    NULL
#define DEFAULT_OPT_PORT   8000
#define DEFAULT_OPT_SIZE   32
#define DEFAULT_OPT_COUNT  1
#define DEFAULT_OPT_RVB    0
#define DEFAULT_OPT_SDB    0
#define DEFAULT_OPT_SYNC   false
#define DEFAULT_OPT_MCAST  false
#define DEFAULT_OPT_MTUDIS false
#define DEFAULT_OPT_RSE    false
#define DEFAULT_OPT_DLY    0
#define DEFAULT_OPT_CON    false
#define DEFAULT_OPT_FLABEL 0

#define OP_RECV         'R'
#define OP_RECV_LONG    "recv"
#define OP_FROM         'F'
#define OP_FROM_LONG    "from"
#define OP_SPORT        'z'
#define OP_SPORT_LONG   "sport"
#define OP_DST          'S'
#define OP_DST_LONG     "sendto"
#define OP_PORT         'p'
#define OP_PORT_LONG    "port"
#define OP_SIZE         'l'
#define OP_SIZE_LONG    "size"
#define OP_COUNT        'c'
#define OP_COUNT_LONG   "count"
#define OP_DLY          'd'
#define OP_DLY_LONG     "delay"
#define OP_RVB          'r'
#define OP_RVB_LONG     "recvbuf"
#define OP_SDB          's'
#define OP_SDB_LONG     "sendbuf"
#define OP_MCAST        'm'
#define OP_MCAST_LONG   "mcast"
#define OP_MTUDIS       'n'
#define OP_MTUDIS_LONG  "mtudisc"
#define OP_SYNC         'y'
#define OP_SYNC_LONG    "synchar"
#define OP_RSE          'r'
#define OP_RSE_LONG     "reuse"
#define OP_CON          'C'
#define OP_CON_LONG     "connect"
#define OP_HLP          'h'
#define OP_HLP_LONG     "help"
#define OP_FLABEL       'X'
#define OP_FLABEL_LONG  "flabel"

#define OPTION_STRING "RF:z:S:p:l:c:d:r:s:X:bnryCh"

int parse_arguments(/*@ in @*/  int  argc, 
	 	    /*@ in @*/  char *argv[],
		    /*@ out @*/ bool *recvr,
		    /*@ out @*/ char **from,
		    /*@ out @*/ int  *sport,
		    /*@ out @*/ char **dst,
		    /*@ out @*/ int  *port,
		    /*@ out @*/ int  *size,
		    /*@ out @*/ int  *count,
		    /*@ out @*/ bool *sync,
                    /*@ out @*/ int  *delay,
		    /*@ out @*/ int  *recvbuf,
		    /*@ out @*/ int  *sendbuf,
		    /*@ out @*/ bool *mcast,
		    /*@ out @*/ bool *mtudis,
		    /*@ out @*/ unsigned int *flabel,
		    /*@ out @*/ bool *reuse,
		    /*@ out @*/ bool *conn)
{
  int  res;
  static struct option long_options[] = 
    {
      {OP_FROM_LONG,   1, 0, (int) OP_FROM},
      {OP_SPORT_LONG,  1, 0, (int) OP_SPORT},
      {OP_DST_LONG,    1, 0, (int) OP_DST},
      {OP_PORT_LONG,   1, 0, (int) OP_PORT},
      {OP_SIZE_LONG,   1, 0, (int) OP_SIZE},
      {OP_COUNT_LONG,  1, 0, (int) OP_COUNT},
      {OP_SYNC_LONG,   0, 0, (int) OP_SYNC},
      {OP_DLY_LONG,    1, 0, (int) OP_DLY},
      {OP_RVB_LONG,    1, 0, (int) OP_RVB},
      {OP_SDB_LONG,    1, 0, (int) OP_SDB},
      {OP_MCAST_LONG,  0, 0, (int) OP_MCAST},
      {OP_MTUDIS_LONG, 0, 0, (int) OP_MTUDIS},
      {OP_RSE_LONG,    0, 0, (int) OP_RSE},
      {OP_CON_LONG,    0, 0, (int) OP_CON},
      {OP_HLP_LONG,    0, 0, (int) OP_HLP},
      {OP_FLABEL_LONG, 0, 0, (int) OP_FLABEL},
      {0, 0, 0, 0}
    };
  *recvr   = DEFAULT_OPT_RECV; 
  *from    = DEFAULT_OPT_FROM; 
  *sport   = DEFAULT_OPT_SPORT;
  *dst     = DEFAULT_OPT_DST; 
  *port    = DEFAULT_OPT_PORT;
  *size    = DEFAULT_OPT_SIZE;
  *count   = DEFAULT_OPT_COUNT;  
  *delay   = DEFAULT_OPT_DLY;
  *recvbuf = DEFAULT_OPT_RVB;
  *sendbuf = DEFAULT_OPT_SDB; 
  *sync    = DEFAULT_OPT_SYNC; 
  *mcast   = DEFAULT_OPT_MCAST;
  *mtudis  = DEFAULT_OPT_MTUDIS;
  *flabel  = DEFAULT_OPT_FLABEL;
  *reuse   = DEFAULT_OPT_RSE;
  *conn    = DEFAULT_OPT_CON;
  
  while ( (res = getopt_long(argc, argv, OPTION_STRING, long_options, NULL)) != -1 )
    // while ( (res = getopt(argc, argv, OPTION_STRING)) != -1 )
    {
      if  (res == (int) OP_RECV)
	{
          *recvr = true;
	}
      else if  (res == (int) OP_FROM)
	{
          //*from =  cc_get_str_address(optarg);
          *from = optarg;
	}
      else if  (res == (int) OP_SPORT)
	{
	  *sport = atoi(optarg);
	}
      if  (res == (int) OP_DST)
	{
          //*dst =  cc_get_str_address(optarg);
          *dst = optarg;
	}
      else if  (res == (int) OP_PORT)
	{
	  *port = atoi(optarg);
	}
      else if  (res == (int) OP_SIZE)
	{
	  *size = atoi(optarg);
	}
      else if  (res == (int) OP_COUNT)
	{
	  *count = atoi(optarg);
	}
      else if  (res == (int) OP_SYNC)
	{
	  *sync = true;
	}
      else if  (res == (int) OP_DLY)
	{
	  *delay = atoi(optarg);
	}
      else if  (res == (int) OP_RVB)
	{
	  *recvbuf = atoi(optarg);
	}
      else if  (res == (int) OP_SDB)
	{
	  *sendbuf = atoi(optarg);
	}
      else if  (res == (int) OP_MCAST)
	{
	  *mcast = true;
	}
      else if  (res == (int) OP_MTUDIS)
	{
	  *mtudis = true;
	}
      else if  (res == (int) OP_FLABEL)
	{
	  *flabel = atoi(optarg);
	}
      else if  (res == (int) OP_RSE)
	{
	  *reuse = true;
	}
      else if  (res == (int) OP_CON)
	{
	  *conn = true;
	}
      else if  (res == (int) OP_HLP)
	{
	  return (int) true;
	}
    }
  //  Recvr +  SYNC + DST  : envia primero un char
  // !Recvr +  SYNC + NULL : espera y luego envia
  //  Recvr + !SYNC + NULL : solo espera
  // !Recvr + !SYNC + DST  : debe enviar        
  return (int) ( (*from == NULL)  || (*sport < 0)  || (*sport > 65535) || (*size < 0) ||
                 (*count < 0)     || (*port < 0)   || (*port  > 65535) || 
                 ((*recvr)&&(*sync)&&(*dst==NULL)) || ((!*recvr)&&(*sync)&&(*dst!=NULL)) ||
                 ((*recvr)&&(!*sync)&&(*dst!=NULL)) || ((!*recvr)&&(!*sync)&&(*dst==NULL)) );
}

/***
 * PRIVATE:
 *
 * Muestra un mensaje indicando como se utiliza el programa
 *
 * arg:  command line first argument
 * code: EXIT_SUCCESS|EXIT_FAILURE
 */
void usage(/*@ in @*/ const char *arg, 
                             int code)
{
  printf("usage: %s { {-R|--recv} || {-R|--recv} {-y|--synchar} {-S|--sendto} <dst-ip> ||| \n",arg);
  printf("            {-S|-sendto} <dst-ip> || {-y|--synchar} }\n");
  printf("          [-F|--from <src-ip>] [-z|--sport <port>] [-p|--port  <port>] \n");
  printf("          [-l|--size <size>] [-c|--count <count>]  [-X|--flabel <label>] \n");
  printf("          [-d|--delay <usec>] [-r|--reuse] [-C|--connect] \n");
  printf("          [-m|--mcast] [-n|--mtudisc]  [-r|--recvbuf <size>] [-s|--sendbuf <size>] \n");
  printf("          [--help|-h]\n");
  printf("default sport %d, default from %s, default port %d, default size %d\n",
	 DEFAULT_OPT_SPORT, DEFAULT_OPT_FROM, DEFAULT_OPT_PORT, DEFAULT_OPT_SIZE);
  printf("default count %d, default delay %d \n",DEFAULT_OPT_COUNT, DEFAULT_OPT_DLY);
  if ( code != 0 )
    {
      exit(code);
    }
}

/*** # EOF # ***/
