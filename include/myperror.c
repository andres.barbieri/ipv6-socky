/**   
 *------------------------------------------------------------------------
 *
 * FILE NAME:   myperror.c
 *
 * PURPOSE:     
 *
 * Para el Magister de Sistemas Distribuidos           
 *
 * FILE REFERENCES:        
 *
 *            Name               I/O       Description             
 *            ----               ---       -----------
 * @file      <fname>            <i|o|io>  <fdesc>
 * 
 * EXTERNAL REFERENCES:
 *
 *            Name               I/O       Description             
 *            ----               ---       -----------
 * @external  <eref>             <i|o|io>  <edesc> 
 *
 * MAIN: 
 *
 * $Author:   Andres Barbieri$ barbieri@cespi.unlp.edu.ar
 *
 * $Revision: 0.1$
 *
 * $Date:     2006/05/01$
 *
 * REQUIREMENTS/FUNCTIONAL SPECIFICATIONS:
 *
 * Proveer una funcion para manejar/mostrar los errores
 * de una forma thread safe/reentrante.
 * 
 * NOTES:
 *
 * The integer errno is set by system calls (and some library functions)
 * to indicate what went wrong. It is thread-local; setting it in one thread 
 * does not affect its value in any other thread.
 *
 * <errno.h>
 * extern int    errno;
 * POSIX.1 
 *
 * <errno.h> 
 * extern lvalue errno;
 * ISO C 
 *
 * -------------------------------------------------------------------
 *
 * Returns a string describing the error code passed in the argument errnum,
 * It string lives in a static user context space. It is no thread-safe.
 *
 * <string.h>
 * char *strerror(int errnum);
 * ISO C (C89), SVID 3, BSD 4.3, POSIX.1 
 *
 * For thread-safe use:
 * <string.h>
 * int strerror_r(int errnum, char *buf, size_t n);
 * SUSv3 (Digital Unix, HP Unix)
 *
 * Not need user-suplied buffer
 * <string.h>
 * char *strerror_r(int errnum, char *buf, size_t n);
 * GNU extension glibc-2 
 *
 * The calls conform POSIX IEEE Std 1003.1c
 *
 * Las implementaciones acutales POSIX tienden a devolver el c�digo
 * de error directamente desde la invocaci�n.
 * http://www.unix.org/whitepapers/reentrant.html
 *
 * DEVELOPMENT HISTORY:
 *
 * Date        Author     Revision    Release   Description  
 * ----        ------     --------    -------   -----------
 * 2006/05/01  andres          0.1       -      Start
 *
 *------------------------------------------------------------------------
 */


#include <errno.h>  // errno
#include <stdio.h>  // fprintf
#include <string.h> // strerror

#define _MYPERROR_C

extern int errno;

/**
 * Handle Error Messages in a reentrant mode
 *
 */

static void my_perror_r(const char *s)
{
#ifdef SUSV3
  char stre[32];
  if ( strerror_r(errno, stre, 32) < 0 )
  {
    stre[0] = '\0';
  }
#else
#ifdef _GNU_SOURCE
  char *stre;
  stre = strerror_r(errno, NULL, 0);
  fprintf(stderr,"%s: %s\n",s,stre);
#else
  int stre;
  stre = strerror_r(errno, NULL, 0);
  fprintf(stderr,"%s: %d\n",s,stre);
#endif  
#endif
}

/*** # EOF # ***/
