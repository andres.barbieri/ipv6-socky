#include "parse_arguments.h"

/**
 * Mode arguments
 */

#define OP_CLI_MODE "-C"
#define OP_LIS_MODE "-L"
#define OP_CLI_SZ    (2)
#define OP_LIS_SZ    (2)

/**
 * Arguments default values and parameter indicators for Listen/Server mode
 */


#define DEFAULT_OPT_BLO       1
#define DEFAULT_OPT_FORKING   0

#define OP_BLO          'b'
#define OP_BLO_LONG     "blog"
#define OP_FRK          'f'
#define OP_FRK_LONG     "fork"
#define OP_THR          't'
#define OP_THR_LONG     "thread"
#define OP_UNQ          'u'
#define OP_UNQ_LONG     "unique"

//#define LIS_OPTION_STRING "b:"

/**
 * Arguments default values and parameter indicators for Client mode
 */

#define DEFAULT_OPT_DST    NULL
#define DEFAULT_OPT_SPORT  0
#define DEFAULT_OPT_FLABEL 0

#define OP_DST          'd'
#define OP_DST_LONG     "dst"
#define OP_SPORT        'z'
#define OP_SPORT_LONG   "sport"
#define OP_FLABEL       'X'
#define OP_FLABEL_LONG  "flabel"
#define OP_CGCTRL       'G'
#define OP_CGCTRL_LONG  "cgtrl"

//#define CLI_OPTION_STRING "d:z:f"

/**
 * Common arguments default values and parameter indicators 
 */

#define DEFAULT_OPT_LOCAL  "0::"
#define DEFAULT_OPT_PORT   8000
#define DEFAULT_OPT_DLY    0
#define DEFAULT_OPT_RVB    0
#define DEFAULT_OPT_SDB    0
#define DEFAULT_OPT_MSS    0
#define DEFAULT_OPT_RSE    false
#define DEFAULT_OPT_NDE    false
#define DEFAULT_OPT_KEA    false

#define OP_LOCAL        'l'
#define OP_LOCAL_LONG   "local"
#define OP_PORT         'p'
#define OP_PORT_LONG    "port"

#define OP_DLY          'w'
#define OP_DLY_LONG     "wait"
#define OP_RVB          'R'
#define OP_RVB_LONG     "recvbuf"
#define OP_SDB          'S'
#define OP_SDB_LONG     "sendbuf"
#define OP_MSS          'm'
#define OP_MSS_LONG     "mss"

#define OP_RSE          'r'
#define OP_RSE_LONG     "reuse"
#define OP_NDE          'n'
#define OP_NDE_LONG     "nodelay"
#define OP_KEA          'k'
#define OP_KEA_LONG     "keepalive"

#define OP_HLP          'h'
#define OP_HLP_LONG     "help"

#define OP_EXTRA        'E'
#define OP_EXTRA_LONG   "extra"

#define OP_EXTRA_FLAG        "-E"
#define OP_EXTRA_LONG_FLAG   "--extra"
#define OP_EXTRA_SZ          (2) 
#define OP_EXTRA_LONG_SZ     (7)


#define OPTION_STRING "CLEl:p:w:R:S:m:rnkhd:z:X:G:b:ftu"


static struct option long_options[] = 
              {
              {OP_LOCAL_LONG, 1, 0, (int) OP_LOCAL},
              {OP_PORT_LONG,  1, 0, (int) OP_PORT},
              {OP_DLY_LONG,   1, 0, (int) OP_DLY},
              {OP_RVB_LONG,   1, 0, (int) OP_RVB},
              {OP_SDB_LONG,   1, 0, (int) OP_SDB},
              {OP_MSS_LONG,   1, 0, (int) OP_MSS},
              {OP_RSE_LONG ,  0, 0, (int) OP_RSE},
              {OP_NDE_LONG ,  0, 0, (int) OP_NDE},
              {OP_KEA_LONG ,  0, 0, (int) OP_KEA},
	      {OP_DST_LONG,   1, 0, (int) OP_DST},
	      {OP_SPORT_LONG, 1, 0, (int) OP_SPORT},
	      {OP_BLO_LONG,   1, 0, (int) OP_BLO},
	      {OP_FRK_LONG,   0, 0, (int) OP_FRK},
	      {OP_THR_LONG,   0, 0, (int) OP_THR},
	      {OP_UNQ_LONG,   0, 0, (int) OP_UNQ},
              {OP_EXTRA_LONG, 0, 0, (int) OP_EXTRA},
              {OP_HLP_LONG,   0, 0, (int) OP_HLP},
              {OP_FLABEL_LONG,0, 0, (int) OP_FLABEL},
              {OP_CGCTRL_LONG,0, 0, (int) OP_CGCTRL},
              {0, 0, 0, 0}
              };

/**
 * Parse Common arguments
 */

static int parse_common_arguments(/*@ in @*/  int  argc, 
	 		          /*@ in @*/  char *argv[],
			          /*@ out @*/ char **local,
			          /*@ out @*/ int  *port,
                                  /*@ out @*/ int  *delay,
                                  /*@ out @*/ int  *recvbuf,
                                  /*@ out @*/ int  *sendbuf,
                                  /*@ out @*/ int  *mss,
			          /*@ out @*/ bool *reuse,
		                  /*@ out @*/ bool *nodelay,
		                  /*@ out @*/ bool *keepalive)
{
     int  res;
     *local     = DEFAULT_OPT_LOCAL; 
     *port      = DEFAULT_OPT_PORT;
     *delay     = DEFAULT_OPT_DLY;
     *recvbuf   = DEFAULT_OPT_RVB;
     *sendbuf   = DEFAULT_OPT_SDB;
     *mss       = DEFAULT_OPT_MSS;
     *reuse     = DEFAULT_OPT_RSE;
     *nodelay   = DEFAULT_OPT_NDE;
     *keepalive = DEFAULT_OPT_KEA;

     while ( (res = getopt_long(argc, argv, OPTION_STRING, long_options, NULL)) != -1 )
     // while ( (res = getopt(argc, argv, OPTION_STRING)) != -1 )
     {
      if  (res == (int) OP_LOCAL)
	{
	  //*local = cc_get_str_address(optarg); 
          *local = optarg; 
	}
      else if  (res == (int) OP_PORT)
	{
	  *port = atoi(optarg);
	}
      else if  (res == (int) OP_DLY)
	{
	  *delay = atoi(optarg);
	}
      else if  (res == (int) OP_RVB)
	{
	  *recvbuf = atoi(optarg);
	}
      else if  (res == (int) OP_SDB)
	{
	  *sendbuf = atoi(optarg);
	}
      else if  (res == (int) OP_MSS)
	{
	  *mss = atoi(optarg);
	}
      else if  (res == (int) OP_RSE)
	{
	  *reuse = true;
	}
      else if  (res == (int) OP_NDE)
	{
	  *nodelay = true;
	}
      else if  (res == (int) OP_KEA)
	{
	  *keepalive = true;
	}
      else if (res == (int) OP_EXTRA)
	{
	  break;
	}
      else if  (res == (int) OP_HLP)
	{
	  return (int) true;
	}
     }
  return (int) ( (*local == NULL) || (*port < 0)    || (*port > 65535) || 
                 (*delay < 0)     || (*recvbuf < 0) || (*sendbuf < 0)  || (*mss < 0) );
}


/**
 * Parse Listen/Server only arguments
 */

static int parse_listen_arguments(/*@ in @*/  int  argc, 
	 		          /*@ in @*/  char *argv[],
			          /*@ out @*/ int  *blog,
				  /*@ out @*/ int  *forking)
{
  int  res;
  *blog      = DEFAULT_OPT_BLO; 
  *forking   = DEFAULT_OPT_FORKING;

  /** Reinit Argument Parsing **/  
  optind = 0;

  while ( (res = getopt_long(argc, argv, OPTION_STRING, long_options, NULL)) != -1 )
    // while ( (res = getopt(argc, argv, OPTION_STRING)) != -1 )
    {
      if  (res == (int) OP_BLO)
	{
	  *blog = atoi(optarg);
	}
      else if  (res == (int) OP_FRK)
	{
	  *forking = 1;
	}
      else if  (res == (int) OP_THR)
	{
	  *forking = 2;
	}
      else if  (res == (int) OP_UNQ)
	{
	  *forking = 0;
	}
    }
  return (int) (*blog < 0);
}


/**
 * Parse Listen/Server only arguments
 */

static int parse_client_arguments(/*@ in @*/  int           argc, 
	         		  /*@ in @*/  char          *argv[],
			          /*@ out @*/ char          **dst,
			          /*@ out @*/ int           *sport,
				  /*@ out @*/ char          **cgctrl,
                                  /*@ out @*/ unsigned int  *flabel)
{
  int  res;
  
  *dst     = DEFAULT_OPT_DST; 
  *sport   = DEFAULT_OPT_SPORT;  
  *flabel  = DEFAULT_OPT_FLABEL;

  /** Reinit Argument Parsing **/  
  optind = 0;

  while ( (res = getopt_long(argc, argv, OPTION_STRING, long_options, NULL)) != -1 )
    //while ( (res = getopt(argc, argv, OPTION_STRING)) != -1 )
    {
      if  (res == (int) OP_DST)
	{
	  //*dst = cc_get_str_address(optarg);
          *dst = optarg;
	}
      else if  (res == (int) OP_SPORT)
	{
	  *sport = atoi(optarg);
	}
      else if  (res == (int) OP_CGCTRL)
	{
	  *cgctrl = optarg;
	}
      else if  (res == (int) OP_FLABEL)
	{
	  *flabel = atoi(optarg);
	}
    }  
  return (int) ( (*dst == DEFAULT_OPT_DST) || (*sport < 0) || 
                 (*sport > 65535) || (*flabel > 1048575) );
}



static int parse_mode_arguments(/*@ in @*/  int  argc, 
	                        /*@ in @*/  char *argv[],
		                /*@ out @*/ bool *lismode)
{
  int result;

  *lismode = false;

  if (argc < 2)
    {
      result = -1;
    }
  else if (strncmp(argv[1], OP_LIS_MODE, OP_LIS_SZ) == 0)
    {
     *lismode = true;
     result = 0;
    } 
  else if (strncmp(argv[1], OP_CLI_MODE, OP_CLI_SZ) == 0)
    {
      *lismode = false;
      result = 0;
    } 
  else
    {
      result = -1;
    } 
  return result;
}


int parse_until_extra_arguments(/*@ inout @*/  int  *argc, 
	 	                /*@ inout @*/  char **argv[])
{
  int i;
  bool found_Extra = false;

  for(i=1; (i<(*argc))&&(!found_Extra); i++)
    {
       if ( (strncmp( (*argv)[i], OP_EXTRA_FLAG, OP_EXTRA_SZ) == 0)||
	    (strncmp( (*argv)[i], OP_EXTRA_LONG_FLAG, OP_EXTRA_LONG_SZ) == 0) )
	 {
	   (*argc)     = (*argc) - i; //(i + 1);
	   (*argv)     = (*argv) + i; //(i + 1);
	   found_Extra = true;
	 }
    }
  if (! found_Extra)
    *argc = 0;
  return (int) found_Extra;
}
				
int parse_arguments(/*@ in @*/  int  argc, 
	 	    /*@ in @*/  char *argv[],
		    /*@ out @*/ char **local,
		    /*@ out @*/ int  *port,
                    /*@ out @*/ int  *delay,
                    /*@ out @*/ int  *recvbuf,
                    /*@ out @*/ int  *sendbuf,
                    /*@ out @*/ int  *mss,
		    /*@ out @*/ bool *reuse,
		    /*@ out @*/ bool *nodelay,
		    /*@ out @*/ bool *keepalive,
		    /*@ out @*/ int  *blog,
		    /*@ out @*/ int  *forking,
 	            /*@ out @*/ char **dst,
	            /*@ out @*/ int  *sport,
	            /*@ out @*/ char **cgctrl,
                    /*@ out @*/ unsigned int *flabel,
                    /*@ out @*/ bool *lismode)
                    
{
  int result;

  if (parse_mode_arguments(argc, argv, lismode) != 0)
    {
      result = -1;
    }
  else
    {
      if (parse_common_arguments(argc, argv, local, port, delay, recvbuf, sendbuf, 
                                 mss, reuse, nodelay, keepalive ) != 0)
	{
	  result = -1;
	}
      else
	{
	  if (*lismode)
	    {
	      if ( parse_listen_arguments(argc, argv, blog, forking) != 0)
		{
		  result = -1;
		} 
	      else
		{
		  result = 0;
		}
	    }
	  else
	    {
	      if ( parse_client_arguments(argc, argv, dst, sport, cgctrl, flabel) != 0)
		{
		  result = -1;
		} 
	      else
		{
		  result = 0;
		}
	    }
	}
    }
  return result;
}


/**
 * arg:  command line first argument
 * code: EXIT_SUCCESS|EXIT_FAILURE
 */

void usage(/*@ in @*/const char *arg, int code)
{
    printf("usage: %s {-C|-L} \n",arg);
    printf("          [-l|--local <src-ip>] [-p|--port <port>] \n");
    printf("          [-w|--wait <sec>] [-R|--recvbuf <size>] [-S|--sendbuf <size>]\n");
    printf("          [-m|--mss <size>] [-r|--reuse]  [-n|--nodelay] [-k|--keepalive]\n");
    printf("          [--help|-h]\n");
    printf("  For -L: [-b|--blog <num>] [-f|--fork|-t|--thread|-u|--unique] \n");
    printf("  For -C: {-d|--dst} <dst-ip> [-z|--sport <port>] [-X|--flabel <label>]\n");
    printf("                              [-G|--cgctrl {reno|vegas|cubic|...}]\n");
    printf("  -L stands for Listen (Server Mode), -C stands for Client\n");
    printf("For both, extra options\n");
    printf("          [-E|--extra <arg1> <arg2> ... <argn>] \n");
    printf("  Extra args may be: +                         (interactive)\n");
    printf("                     h [times] [delay]         (hole by char)\n");
    printf("                     k [times] [chunk]         (hole by chunk)\n");
    printf("                     e [times] [delay] [tmout] (echo chars and delay or timeout)\n");
    printf("                     p [times] [delay] [tmout] (print chars and delay or timeout)\n");
    printf("                     C [times] [delay] [tmout] (gen seq chars delay and tmout)\n");
    printf("                     R [times] [delay]         (gen random chars)\n"); 
    printf("                     A [times] [delay] [rchar] (rep chars delay)\n");
    printf("                     K [times] [chunk] [tmout] (gen chars by chunk)\n");
    printf("                     T [times] [delay]         (gen time)\n");
    printf("                     times = 0 means infinity \n");   
    printf("                     delay = 0 means no delay \n");   
    printf("                     tmout = 0 means no timeout \n");   
    if ( code != 0 )
    {
         exit(code);
    }
}

/*** # EOF # ***/
