/**   
 *------------------------------------------------------------------------
 *
 * FILE NAME:   errshowmine.c
 *
 * PURPOSE:     
 *
 * Para el Magister de Sistemas Distribuidos           
 *
 * FILE REFERENCES:        
 *
 *            Name               I/O       Description             
 *            ----               ---       -----------
 * @file      <fname>            <i|o|io>  <fdesc>
 * 
 * EXTERNAL REFERENCES:
 *
 *            Name               I/O       Description             
 *            ----               ---       -----------
 * @external  <eref>             <i|o|io>  <edesc> 
 *
 * MAIN: 
 *
 * $Author:   Andres Barbieri$ barbieri@cespi.unlp.edu.ar
 *
 * $Revision: 0.1$
 *
 * $Date:     2006/05/01$
 *
 * REQUIREMENTS/FUNCTIONAL SPECIFICATIONS:
 *
 * Manejo de errores de forma reentrante.
 * 
 * NOTES:
 *
 * The  integer errno is set by system calls (and some library functions)
 * to indicate what went wrong. It is thread-local; setting it in one thread 
 * does not affect its value in any other thread.
 *
 * <errno.h>
 * extern int    errno;
 * POSIX.1 
 *
 * <errno.h> 
 * extern lvalue errno;
 * ISO C 
 *
 * -------------------------------------------------------------------
 *
 * Returns a string describing the error code passed in the argument errnum,
 * It string lives in a static user context space. It is no thread-safe.
 *
 * <string.h>
 * char *strerror(int errnum);
 * ISO C (C89), SVID 3, BSD 4.3, POSIX.1 
 *
 * For thread-safe use:
 * <string.h>
 * int strerror_r(int errnum, char *buf, size_t n);
 * SUSv3 (Digital Unix, HP Unix)
 *
 * Not need user-suplied buffer
 * <string.h>
 * char *strerror_r(int errnum, char *buf, size_t n);
 * GNU extension glibc-2 
 *
 * The calls conform POSIX IEEE Std 1003.1c
 *
 * Las implementaciones acutales POSIX tienden a devolver el c�digo
 * de error directamente desde la invocaci�n de la funci�n.
 * http://www.unix.org/whitepapers/reentrant.html
 * 
 *
 * DEVELOPMENT HISTORY:
 *
 * Date        Author     Revision    Release   Description  
 * ----        ------     --------    -------   -----------
 * 2006/05/01  andres          0.1       -      Start
 *
 *------------------------------------------------------------------------
 */

#include <stdio.h>  // printf, sprintf

#include "myperror.c" // my_perror_r

static void do_error(void)
{
     fprintf(stdin,NULL);
}

static void do_no_error(void)
{
}

static void my_perror(const char *s)
{
  fprintf(stderr,"%s: %s\n",s,strerror(errno));
}

int main(int argc, char *argv[])
{
    if ( argc <  2 ) 
      {
      printf("usage: %s {e|.} \n",argv[0]);
      return 1;
      }
    if ( (argc == 2) && ( argv[1][0] == 'e' ) )
      {
        do_error();
      }
    else 
      {
        do_no_error();  
      }
    my_perror("Error at test"); 
    my_perror_r("Error at test");
    return 0;
} 

/*** # EOF # ***/
