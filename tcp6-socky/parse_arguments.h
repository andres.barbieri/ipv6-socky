#ifndef  _PARSE_ARGUMENTS_H 
#define _PARSE_ARGUMENTS_H 

#include <stdio.h>                      /* printf */
#include <stdlib.h>                     /* exit */
#include <getopt.h>                     /* getopt */
#include <stdbool.h>                    /* Booleans */
#include <string.h>                     /* strncmp */

//#ifndef _MYRESOLV_H
//#include "../myresolv.h"
//#endif

int parse_arguments(/*@ in @*/  int  argc, 
	 	    /*@ in @*/  char *argv[],
		    /*@ out @*/ char **local,
		    /*@ out @*/ int  *port,
                    /*@ out @*/ int  *delay,
                    /*@ out @*/ int  *recvbuf,
                    /*@ out @*/ int  *sendbuf,
                    /*@ out @*/ int  *mss,
		    /*@ out @*/ bool *reuse,
		    /*@ out @*/ bool *nodelay,
		    /*@ out @*/ bool *keepalive,
		    /*@ out @*/ int  *blog,
	    	    /*@ out @*/ int  *forking,
 	            /*@ out @*/ char **dst,
	            /*@ out @*/ int  *sport,
	            /*@ out @*/ char **cgctrl,
                    /*@ out @*/ unsigned int *flabel,
                    /*@ out @*/ bool *lismode);


void usage(/*@ in @*/const char *arg, int code);

#endif

/*** # EOF # ***/

