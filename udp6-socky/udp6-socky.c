/**   
 *------------------------------------------------------------------------
 *
 * FILE NAME:   udp6-socky.c
 *
 * PURPOSE:     
 *
 * Para la tesis del Magister de Redes 
 *
 * FILE REFERENCES:        
 *
 *            Name               I/O       Description             
 *            ----               ---       -----------
 * @file      <fname>            <i|o|io>  <fdesc>
 * 
 * EXTERNAL REFERENCES:
 *
 *            Name               I/O       Description             
 *            ----               ---       -----------
 * @external  <eref>             <i|o|io>  <edesc> 
 *
 * MAIN: 
 *
 * $Author:   Andres Barbieri$ barbieri@cespi.unlp.edu.ar
 *
 * $Revision: 0.1$
 *
 * $Date:     2014/06/21$
 *
 * REQUIREMENTS/FUNCTIONAL SPECIFICATIONS:
 *
 * Ver par�metros de una comunicaci�n UDP (RFC768). 
 *
 * NOTES:
 *
 * Utiliza las llamadas est�ndares de SOCKET BSD.
 * All functions: socket(), sendto(), recvfrom(), send(),
 * recv() calls conforms 4.4BSD, SUSv2, POSIX 1003.1-2001.  
 * These function calls appeared in 4.2BSD.
 * These function calls follow the Single Unix Specification (SUS), as
 * glibc2 also does.
 *
 * Para correr ver notas m�s abajo (A�n no se agreg�).
 *
 * DEVELOPMENT HISTORY:
 *
 * Date        Author     Revision    Release   Description  
 * ----        ------     --------    -------   -----------
 * 2006/04/01  andres          0.1       -      Start
 *
 *------------------------------------------------------------------------
 */

#include <unistd.h>                     /* close , read */
#include <sys/types.h>                  /* connect , socket */
#include <sys/socket.h>                 /* socket , send, recv, ... */
#include <arpa/inet.h>                  /* ip socket , sockaddr_in6 inet_pton */
#include <stdlib.h>                     /* exit , malloc , free  */
#include <stdio.h>                      /* printf , stderr, perror*/
#include <errno.h>                      /* errno  */
#include <getopt.h>                     /* getopt */
#include <stdbool.h>                    /* Booleans */
#include "parse_arguments.h"            /* parse_arguments */

#include <options.h>                    /* set_<OPT> */

/**
 * Handle Error Messages in a reentrant mode
 */
#ifndef _MYPRERROR_C
#include <myperror.c>
#endif

/***
 * PRIVATE:
 *
 * Hace una espera de "delay" segundos mostrando
 * el avance en la terminal.
 *
 */
static void show_sleep(int delay)
{
  int i;

  if (delay == 0) 
    return;
  printf("Sleeping for %d sec ",delay);
  fflush(stdout);
  for(i=0;i<delay;i++)
    {
      sleep(1);
      printf(".");
      fflush(stdout);
    }
  printf("\n");
}

/***
 * PRIVATE:
 *
 * Muestra las Opciones de la Conexion
 *
 */
static void show_options(int sock)
{
   fprintf(stderr,"RBUF:      %d\n",get_rbufsize(sock));
   fprintf(stderr,"SBUF:      %d\n",get_sbufsize(sock));
   fprintf(stderr,"MCAST:     %d\n",(int) get_mcast(sock));
   fprintf(stderr,"REUSE:     %d\n",(int) get_reuseaddr(sock));
   fprintf(stderr,"MTUDISC:   %d\n",get_mtu_discover(sock));
}

/***
 * PRIVATE:
 *
 * Genera Chars 
 *
 */
/*@null@*/ static char *gen_data(/*@in out@*/ /*@null@*/ int *size)
{
  char *buf = NULL;  
  register int i;

  buf = (char*) malloc(sizeof(char)*(*size));
  if (buf == (char*) NULL)
    {
      *size = 0;
      return  NULL;
    }
  for(i=0;i<*size;i++)
    {
    buf[i] = (char) ((i % 94) + 33);
    }
  return buf;
}

/************************
 *
 * MAIN PROGRAM
 *
 ************************/

int main(int argc, char *argv[])
{
    int                 sock; 
    //struct sockaddr_in addr;
    struct sockaddr_in6 addr;
    int  addrlen;
    bool recvr    = false;
    char *buf     = NULL;
    char *from    = NULL;
    int  sport    = 0;
    char *dst     = NULL;
    int  port     = 0;
    int  size     = 0;
    int  count    = 0;
    bool sync     = false;
    int  delay    = 0;
    int  recvbuf  = 0;
    int  sendbuf  = 0;
    bool mcast    = false;
    bool mtudis   = false;
    unsigned int flabel  = 0;
    bool reuse    = false;
    bool conn     = false;
    int  rval     = 1;
    int  i        = 0;    
    char c        = '0';
    char tmp[64];

    /****************** COMUN ********************/	      

    /***
     * Parsea los argumentos
     */
    if ( parse_arguments(argc, argv, &recvr, &from, &sport, &dst, &port, &size, &count, 
                         &sync, &delay, &recvbuf, &sendbuf, &mcast, &mtudis, &flabel, &reuse, &conn) != 0 )
	{
	  usage(argv[0], EXIT_FAILURE);
	}

    /***
      *  Crea el INET6 Domain/UDP socket       
      */
    //sock = socket(AF_INET, SOCK_DGRAM, 0);
    if ((sock = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP)) < 0)
      {
	perror("ERROR: at opening udp6 socket");
	fflush(stderr);
	exit(EXIT_FAILURE);
      }     
    /***
     * Setea las Opciones Requeridas
     */
    set_rbufsize(recvbuf, sock);
    set_sbufsize(sendbuf, sock);
    if (reuse) 
      {	 
	set_reuseaddr(true, sock);
      }
    if (mtudis)
      {
	set_mtu_discover(sock);
      }
    else
      {
	unset_mtu_discover(sock);
      }
    if (mcast)
      {
	set_mcast(sock);
      }   
    /***
     *  Hace el bind
     */
    /* Create an empty IPv6 socket interface specification */
    (void) memset(&addr, 0, sizeof(addr));
    /* Set up for IPv4 --commented-- */
    //addr.sin_family       = AF_INET;
    //addr.sin_port         = htons((uint16_t) sport);
    // addr.sin_addr.s_addr = inet_addr(from); //htonl(INADDR_ANY);
    /* Set up for IPv6 */
    addr.sin6_family        = AF_INET6;
    addr.sin6_flowinfo      = 0x0;
    addr.sin6_port          = htons((uint16_t) sport);
    if (inet_pton(AF_INET6, from, &(addr.sin6_addr) ) <= 0)
     {
	perror("ERROR: at translating address");
	fflush(stderr);
	exit(EXIT_FAILURE);
     }    
    if ( bind(sock, (struct sockaddr *)&addr, (socklen_t) sizeof(struct sockaddr_in6)) < 0 ) 
      {
	perror("ERROR: at binding to dgram udp6 socket");
	fflush(stderr);
	close(sock);
	exit(EXIT_FAILURE);
      }  

    /***
     * Muestra las opciones del socket
     */
    show_options(sock);

    
    /****************** MODO TX ********************/
    if (!recvr)
      {
	//(void) memset(&addr, 0, sizeof(addr));
	addrlen = (int) sizeof(addr);         
	/** 
	 * Espera SYN CHAR 
	 */
	if (sync)
	  {
	    fprintf(stderr,"Waiting connection on %s:%d reuse:%d\n",from, (int)port, (int)reuse);
	    rval = (int) recvfrom(sock, (char*) &c , sizeof(char), 0,
			          (struct sockaddr*) &addr, (socklen_t*) &addrlen);
	    if ( rval < 0 )
	      {
		perror("ERROR: at accepting incoming udp6 data");
		fflush(stderr);
		close(sock);
		exit(EXIT_FAILURE);
	      }
            // Muestra de donde se conecta 
	    inet_ntop(AF_INET6, (const void*)&(addr.sin6_addr), tmp, sizeof(tmp));
	    fprintf(stderr,"Connected from %s:%d\n", tmp, (int) ntohs(addr.sin6_port));
	  }
       
	/***
         * Genera Datos
         */
	buf  = gen_data(&size);
	if (buf == (char*) NULL)
	  {
	    perror("ERROR: at alloc memory");
	    fflush(stderr);
	    exit(EXIT_FAILURE);
	  }
	while ( ( rval > 0 ) && (i < count) )
	  {
	    if (buf != (char*) NULL)
	      {
		buf[size]='\0';
		if (!sync)
		  {
		    // addr.sin_family     = AF_INET;
		    //addr.sin_port        = htons((uint16_t) port);
		    //addr.sin_addr.s_addr = inet_addr(dst); 		 
		    addr.sin6_family       = AF_INET6;
		    addr.sin6_port         = htons((uint16_t) port);
		    if (inet_pton(AF_INET6, dst, &(addr.sin6_addr) ) <= 0)
		      {
			   perror("ERROR: at translating address");
			   fflush(stderr);
			   exit(EXIT_FAILURE);
		      }    
		  }
		//show_sleep(delay*1000);
		/*** 
		 * Conecta antes de enviar (Modo conectado)
		 */ 
		if (conn)
		  {
		    rval = (int) connect(sock, (struct sockaddr*) &addr, (socklen_t) sizeof(addr));
		    if (rval < 0)
		      {
			perror("ERROR: at connect UDP6");
			fflush(stderr);
			exit(EXIT_FAILURE);		    
		      }
		  }
		if( flabel != 0)
		  {
		    if (set_flabel(flabel, &addr, sock) == -1)
		      {
		    	perror("ERROR: at setting flow label, not exiting ...");
		    	fflush(stderr);
		      }
		  } 
		else
		  {
		    addr.sin6_flowinfo = 0x0;
		  }
		/*** 
		 * Ahora envia los datos 	
		 */ 
		rval = (int) sendto(sock, (char*) buf ,(size_t) size, 0,
			            (struct sockaddr*) &addr, (socklen_t) sizeof(addr)); 
		i++;
		usleep(delay);
		if (rval < 0)
		  {
		    perror("ERROR: at send data");
		    fflush(stderr);
		    exit(EXIT_FAILURE);		    
		  }
	      }
	    else
	      {
		perror("ERROR: at alloc data");
		fflush(stderr);
		exit(EXIT_FAILURE);
	      }
	  }
	// Liberar memoria de Buffer de Datos
	free(buf);
      }
    /****************** MODO RX ********************/
    else //Rx
      {
	/*** 
	 * Envia SYN CHAR 
	 */
	if (sync)
	  {
            /* Set up for IPv4 --commented-- */
	    //addr.sin_family      = AF_INET;
	    //addr.sin_port        = htons((uint16_t) port);
	    //addr.sin_addr.s_addr = inet_addr(dst);
	    /* Set up for IPv6 */
	    addr.sin6_family       = AF_INET6;
	    addr.sin6_port         = htons((uint16_t) port);
	    if (inet_pton(AF_INET6, dst, &(addr.sin6_addr) ) <= 0)
	      {
		perror("ERROR: at translating address");
		fflush(stderr);
		exit(EXIT_FAILURE);
	      }    
	    if( flabel != 0)
	      {
		if (set_flabel(flabel, &addr, sock) == -1)
		  {
		    perror("ERROR: at setting flow label, not exiting ...");
		    fflush(stderr);
		  }
	      } 
	    else
	      {
		addr.sin6_flowinfo = 0x0;
	      }
	    if (sendto(sock, &c, sizeof(char), 0,
		       (struct sockaddr*) &addr, (socklen_t) sizeof(addr))<0) 
	      {
		perror("ERROR: at sending to udp6 socket"); 
		fflush(stderr);
		close(sock);
		exit(EXIT_FAILURE);
	      } 
	  }	  

	/***
	 * Reserva el espacio para el buffer de recepcion
	 */
	buf = (char*) malloc(sizeof(char)*size);
	if ( buf == (char*) NULL )
	  {
	    perror("ERROR: at alloc memory");
	    fflush(stderr);
	    exit(EXIT_FAILURE);
	  }

	/***
	 * Trata de recibir 
	 */
	while ( ( rval > 0 ) && (i < count) )
	  {
	    if ( (rval = (int) recv(sock, buf , (size_t) size , 0)) < 0)
	      {
		perror("ERROR: at reading message from dgram");
		fflush(stderr);
		close(sock);
		exit(EXIT_FAILURE);
	      }
	    else
	      {
		buf[rval] = (char) 0;
		printf("buffer: [%s] size: [%d]\n",buf, rval); 
		i++;
	      }
	  }
	free(buf);
      }
    /************ COMUN *************/
    close(sock);	 
    return (0);
}

/*** # EOF # ***/
