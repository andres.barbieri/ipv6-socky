#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <stdlib.h>
#include <unistd.h>
// #include <stdbool.h>    

#include <sys/types.h>   // socket
#include <sys/socket.h>  // socket

# include <netinet/in.h>
# include <arpa/inet.h>  // inet_pton

# include "flowlabel.h"  // Flowlabel

#define PORT    8000

////////////////////////////////////////////////////////////////////
// set_flow_flowlabel_shared()
int set_flow_label_shared(int sock, struct sockaddr_in6 *addr, int fl)
{
   char freq_buf[sizeof(struct in6_flowlabel_req)];
   struct in6_flowlabel_req *freq = (struct in6_flowlabel_req *)freq_buf;
   int freq_len = sizeof(*freq);
   int so_optval = 1;   

   memset(freq, 0, sizeof(*freq));
   freq->flr_label = htonl(fl & IPV6_FLOWINFO_FLOWLABEL);
   freq->flr_action = IPV6_FL_A_GET;
   freq->flr_flags  = IPV6_FL_F_CREATE;
   //freq->flr_share  = IPV6_FL_S_ANY;
   //freq->flr_share = IPV6_FL_F_CREATE | IPV6_FL_S_EXCL;
   freq->flr_share = IPV6_FL_F_CREATE | IPV6_FL_S_ANY;
   //freq->flr_linger = 1;
   memcpy(&freq->flr_dst, &addr->sin6_addr, 16);
   if (setsockopt(sock, IPPROTO_IPV6, IPV6_FLOWLABEL_MGR, freq, freq_len) == -1) 
     {
       printf("setsockopt MGR: %s\n", strerror(errno));
       return -1;
     }
   // Set Flow label into struct
   addr->sin6_flowinfo = freq->flr_label;
   // Set option to send it
   if (setsockopt(sock, IPPROTO_IPV6, IPV6_FLOWINFO_SEND,&so_optval, sizeof(so_optval)) == -1) 
     {
       printf("setsockopt SEND: %s\n", strerror(errno));
       return -1;
     }
   return 0;
}

////////////////////////////////////////////////////////////////////
// main()
int main(int argc, char *argv[])
{
  int                 sockfd6, i;
    struct sockaddr_in6 dest_sin6;
    int                 port       = ((int)PORT);
    int                 so_optval;
    char                c;

   // Check args
    if (argc < 2)
      {
	printf("usage %s destip, e.g.: %s ::1 [port] \n",argv[0],argv[0]);
	exit(-1);
      }

    if (argc > 2)
      {
	port = atoi(argv[2]);
      }
    /* Create an empty IPv6 socket interface specification */
    (void) memset(&dest_sin6, 0, sizeof(dest_sin6));
    /* Set up for IPv6 */
    dest_sin6.sin6_family = AF_INET6;
    dest_sin6.sin6_port = htons((uint16_t) port);
    if (inet_pton(AF_INET6, argv[1], &(dest_sin6.sin6_addr) ) <= 0)
     {
	perror("ERROR: at translating address");
	fflush(stderr);
	exit(EXIT_FAILURE);
     }    

  /* Create the sockets */
  if ((sockfd6 = socket(PF_INET6, SOCK_STREAM, IPPROTO_TCP)) < 0)
     {
	perror("ERROR: at opening tcp socket");
	fflush(stderr);
	exit(EXIT_FAILURE);
     }

  /* Mark as re-usable (accept more than one connection to use same value) */
  so_optval = 1;
  if (setsockopt(sockfd6, SOL_SOCKET, SO_REUSEADDR, (char *) &so_optval,
		sizeof(SO_REUSEADDR)) < 0)
   {
	perror("ERROR: at setting socket option");
	fflush(stderr);
	close(sockfd6);
	exit(EXIT_FAILURE);

   }

  // Mark Flowlabel
  printf("set FL=%d res=%d\n",0x55,set_flow_label_shared(sockfd6, &dest_sin6,0x55));

  // Try to connect
  if ( connect(sockfd6, (const void*)&dest_sin6, sizeof(dest_sin6)) < 0)
    //(struct sockaddr *) &destaddr, (socklen_t) sizeof(struct sockaddr_in)) < 0)
    {
      perror("ERROR: at connecting to tcp socket");
      fflush(stderr);
      close(sockfd6);
      exit(EXIT_FAILURE);
    }
  
  // Send Data (a bunch of chars)
  for(i=0;i<100;i++)
    {
      c = 'a'; 
      write(sockfd6, &c, (size_t) sizeof(c));
      sleep(1);
    }
  close(sockfd6);
  return (0);
}

/*** EOF ***/
