/**
 *
 * FILENAME: errshow
 *
 * $Author: andres$ barbieri@cespi.unlp.edu.ar
 *
 * $Revision: 0.1$
 *
 * $Date:     2006/05/01$ 
 *
 * Print the last system error (System call error)
 * message on standard error output.
 *
 * <stdio.h>
 * perror(const char *s);
 *
 * ANSI C (ISO C), BSD 4.3, POSIX.1, X/OPEN
 */

#include <stdio.h> // perror, printf

static void do_error(void)
{
  // Format parameter is not known at compile-time.  This can lead to security
  // vulnerabilities because the arguments cannot be type checked.
     fprintf(stdin,NULL);
}

static void do_no_error(void)
{
}

int main(int argc, char *argv[])
{
    if ( argc <  2 ) 
    {
      printf("usage: %s e|. \n",argv[0]);
      return 1;
    }
    if ( (argc == 2) && ( argv[1][0] == 'e' ) )
    {
        do_error();
    }
    else 
    {
        do_no_error();  
    }
    perror("Error at test");
    return 0;
} 

/*** # EOF # ***/
