#include "options.h"
#include "flowlabel.h"
#include <stdio.h>
#include <string.h> /** strnlen **/
#include <errno.h>

int set_rbufsize(unsigned int size, int sock)
{
  unsigned int optbuf = size;
 
  if (size == 0) 
      return 0; 
  return (setsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char*) &optbuf, sizeof(optbuf)));
}

unsigned int get_rbufsize(int sock)
{
  unsigned int        optbuf;
  socklen_t  size = sizeof(optbuf);

  getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char*) &optbuf, &size);
  return (unsigned int) optbuf;
}

int set_sbufsize(unsigned int size, int sock)
{
  unsigned int optbuf = size;

  if (size == 0) 
      return 0; 
  return (setsockopt(sock, SOL_SOCKET, SO_SNDBUF, (char*) &optbuf, sizeof(optbuf)));
}

unsigned int get_sbufsize(int sock)
{
  unsigned int optbuf;
  socklen_t  size = sizeof(optbuf);
 
  getsockopt(sock, SOL_SOCKET, SO_SNDBUF, (char*) &optbuf, &size);
  return (unsigned int) optbuf;
}

int set_nodelay(bool nodelay, int sock)
{
    int opt = (int) nodelay;

    return (setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char*) &opt, sizeof(opt)));
}

bool get_nodelay(int sock)
{
    int opt;
    socklen_t  size = sizeof(opt);

    getsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char*) &opt, &size);
    return (bool) opt;
}   

int set_reuseaddr(bool reuse, int sock)
{
    int opt = (int) reuse;

    return (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*) &opt, sizeof(opt)));
} 

bool get_reuseaddr(int sock)
{
    int opt;
    socklen_t  size = sizeof(opt);

    getsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*) &opt, &size);
    return (bool) opt;
} 

int set_keepalive(bool keepalive, int sock)
{
    int opt = (int) keepalive;

    return (setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, (char*) &opt, sizeof(opt)));
} 

bool get_keepalive(int sock)
{
    int opt;
    socklen_t  size = sizeof(opt);

    getsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, (char*) &opt, &size);
    return (bool) opt;
} 

int set_mss(unsigned int mss, int sock)
{
    unsigned int opt =  mss;
    if (mss == 0) 
      return 0;

    setsockopt(sock, IPPROTO_TCP, TCP_MAXSEG , (char*) &opt, sizeof(opt));
    return setsockopt(sock, SOL_SOCKET, TCP_MAXSEG , (char*) &opt, sizeof(opt));    
} 

unsigned int get_mss(int sock)
{
    unsigned int opt;
    socklen_t  size = sizeof(opt);

    getsockopt(sock, IPPROTO_TCP, TCP_MAXSEG , (char*) &opt, &size);
    return (unsigned int) opt;
} 

int set_flow_label_shared(int sock, struct sockaddr_in6 *addr, unsigned int fl)
{
   char freq_buf[sizeof(struct in6_flowlabel_req)];
   struct in6_flowlabel_req *freq = (struct in6_flowlabel_req *)freq_buf;
   int freq_len = sizeof(*freq);
   int so_optval = 1;   

   memset(freq, 0, sizeof(*freq));
   freq->flr_label = htonl(fl & IPV6_FLOWINFO_FLOWLABEL);
   freq->flr_action = IPV6_FL_A_GET;
   freq->flr_flags  = IPV6_FL_F_CREATE;
   //freq->flr_share = IPV6_FL_F_CREATE | IPV6_FL_S_EXCL;
   freq->flr_share = IPV6_FL_F_CREATE | IPV6_FL_S_ANY;
   //freq->flr_linger = 1;
   memcpy(&freq->flr_dst, &addr->sin6_addr, 16);
   if (setsockopt(sock, IPPROTO_IPV6, IPV6_FLOWLABEL_MGR, freq, freq_len) == -1) 
     {
       printf("setsockopt MGR: %s\n", strerror(errno));
       return -1;
     }
   // Set Flow label into struct
   addr->sin6_flowinfo = freq->flr_label;
   // Set option to send it
   if (setsockopt(sock, IPPROTO_IPV6, IPV6_FLOWINFO_SEND,&so_optval, 
       sizeof(so_optval)) == -1) 
     {
       printf("setsockopt SEND: %s\n", strerror(errno));
       return -1;
     }
   return 0;
}

int set_flow_label_xlusive(int sock, struct sockaddr_in6 *addr, unsigned int fl)
{
   char freq_buf[sizeof(struct in6_flowlabel_req)];
   struct in6_flowlabel_req *freq = (struct in6_flowlabel_req *)freq_buf;
   int freq_len = sizeof(*freq);
   int so_optval = 1;   

   memset(freq, 0, sizeof(*freq));
   freq->flr_label = htonl(fl & IPV6_FLOWINFO_FLOWLABEL);
   freq->flr_action = IPV6_FL_A_GET;
   freq->flr_flags  = IPV6_FL_F_CREATE;
   freq->flr_share = IPV6_FL_F_CREATE | IPV6_FL_S_EXCL;
   //freq->flr_share = IPV6_FL_F_CREATE | IPV6_FL_S_ANY;
   //freq->flr_linger = 1;
   memcpy(&freq->flr_dst, &addr->sin6_addr, 16);
   if (setsockopt(sock, IPPROTO_IPV6, IPV6_FLOWLABEL_MGR, freq, freq_len) == -1) 
     {
       //printf("setsockopt MGR: %s\n", strerror(errno));
       return -1;
     }
   // Set Flow label into struct
   addr->sin6_flowinfo = freq->flr_label;
   // Set option to send it
   if (setsockopt(sock, IPPROTO_IPV6, IPV6_FLOWINFO_SEND,&so_optval, 
       sizeof(so_optval)) == -1) 
     {
       //printf("setsockopt SEND: %s\n", strerror(errno));
       return -1;
     }
   return 0;
}

int  set_flabel(unsigned int flabel, struct sockaddr_in6 *addr, int sock)
{
  return set_flow_label_shared(sock, addr, flabel);
}

unsigned int get_flabel(struct sockaddr_in6 addr, int sock)
{
  return (unsigned int) addr.sin6_flowinfo;
}

int  set_cgctrl(char *cgctrl, int sock)
{
  int optlen = strnlen(cgctrl,20);
  return setsockopt(sock, IPPROTO_TCP, TCP_CONGESTION,  (char*) cgctrl, optlen);
}

int set_hop_limit(unsigned int hl, int sock)
{
  unsigned int optbuf = hl;
 
  //return (setsockopt(sock, SOL_SOCKET, IP_TTL, (char*) &optbuf, sizeof(optbuf)));
  return (setsockopt(sock, IPPROTO_IPV6, IPV6_UNICAST_HOPS,
                     (char *) &optbuf, sizeof(optbuf)) == -1);
}

int set_tos_mindelay(int sock)
{
  unsigned int optbuf = IPTOS_LOWDELAY;
 
  return (setsockopt(sock, SOL_SOCKET, IP_TOS, (char*) &optbuf, sizeof(optbuf)));
}

int set_tos_maxthput(int sock)
{
  unsigned int optbuf = IPTOS_THROUGHPUT;
 
  return (setsockopt(sock, SOL_SOCKET, IP_TOS, (char*) &optbuf, sizeof(optbuf)));
}

int set_mtu_discover(int sock)
{
  unsigned int optbuf = IPV6_PMTUDISC_DO; // tried also IPV6_PMTUDISC_PROBE

  return (setsockopt(sock, IPPROTO_IPV6, IPV6_MTU_DISCOVER, (char*) &optbuf, sizeof(optbuf)));
}

int unset_mtu_discover(int sock)
{
  unsigned int optbuf =  IPV6_PMTUDISC_DONT;
  
  return (setsockopt(sock, IPPROTO_IPV6, IP_MTU_DISCOVER, (char*) &optbuf, sizeof(optbuf)));
}

int get_mtu_discover(int sock)
{
  unsigned int optbuf;
  socklen_t  size = sizeof(optbuf);
 
  getsockopt(sock, IPPROTO_IPV6, IPV6_MTU_DISCOVER, (char*) &optbuf, &size);
  return (int) optbuf;
}

int set_mcast(int sock)
{
  unsigned int opt = 1;
  return -1; 
  //return (setsockopt(sock, SOL_SOCKET, SO_BROADCAST,(char*) &opt, sizeof(opt)));
}

bool get_mcast(int sock)
{
  unsigned int optbuf;
  socklen_t  size = sizeof(optbuf);
 
  //getsockopt(sock, SOL_SOCKET, SO_BROADCAST, (char*) &optbuf, &size);
  //return (bool) optbuf;
  return (bool) false;
}

/**** EOF ****/

