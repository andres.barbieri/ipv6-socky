These are udp6-socky and tcp6-socky programs, tools for testing IPv6 programming API.
They are in very-very beta stage.

Briefly:

  cd tcp6-socky
  make           -- compile tcp6-socky

  cd udp6-socky  
  make           -- compile udp6-socky

There isn't a make install option. In order to install copy the executable binary files where your executables reside. For help, once you have compiled the code, run.
 
 ./tcp6-socky  --help
 ./udp6-socky  --help

Andres Barbieri

