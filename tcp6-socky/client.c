#include <stdlib.h>  /** malloc, free, exit **/
#include <unistd.h>  /** write, close **/
#include <stdio.h>   /** perror, flush **/
#include <stdbool.h> /** Booleans **/ 

#include "common.h"
#include "client.h"


/***
 * NAIVE Client Error Handler
 */
void client_error(client_err_t err)
{
  printf("Error at client side %d\n",err);
}

/***
 * Ejecuta el Cliente
 */
client_err_t  do_client (int fd, 
                         int argc, 
                         /*@ in @*/ char *argv[])
{
  if (do_sth(fd, argc, argv) == 0)
    return CLIENT_OK;
  else
    return -CLIENT_ERRARG;
}

/*** # EOF # ***/
