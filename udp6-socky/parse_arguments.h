#ifndef _PARSE_ARGUMENTS_H 
#define _PARSE_ARGUMENTS_H 

#include <stdio.h>                      /* printf */
#include <stdlib.h>                     /* exit */
#include <getopt.h>                     /* getopt */
#include <stdbool.h>                    /* Booleans */
#include <string.h>                     /* strncmp */

//#ifndef _MYRESOLV_H
//#include "../myresolv.h"
//#endif

int parse_arguments( /*@ in @*/  int  argc, 
		     /*@ in @*/  char *argv[],
		     /*@ out @*/ bool *recvr,
		     /*@ out @*/ char **from,
		     /*@ out @*/ int  *sport,
		     /*@ out @*/ char **dst,
		     /*@ out @*/ int  *port,
		     /*@ out @*/ int  *size,
		     /*@ out @*/ int  *count,
		     /*@ out @*/ bool *sync,
		     /*@ out @*/ int  *delay,
		     /*@ out @*/ int  *recvbuf,
		     /*@ out @*/ int  *sendbuf,
		     /*@ out @*/ bool *mcast,
		     /*@ out @*/ bool *mtudis,
		     /*@ out @*/ unsigned int *flabel,
		     /*@ out @*/ bool *reuse,
		     /*@ out @*/ bool *conn);


void usage(/*@ in @*/const char *arg, int code);

#endif
/*** # EOF # ***/
