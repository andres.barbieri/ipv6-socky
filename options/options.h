#ifndef _OPTIONS_H
#define _OPTIONS_H 

#include <stdbool.h>
#include <sys/types.h>
#include <sys/socket.h>                 /* socket options */
#include <netinet/tcp.h>                /* TCP options    */
#include <netinet/udp.h>                /* UDP options    */
#include <netinet/ip.h>                 /* IP options     */
#include <netinet/in.h>                 
#include <arpa/inet.h>  
#include <string.h>                     /* memset */

/**
 *  man socket: (UDP, TCP)
 *  Sets or gets the maximum socket receive buffer in bytes.  The default value is  
 *  set by the OS (Linux: rmemdefault, rmem_max sysctl)
 */
int set_rbufsize(unsigned int size, int sock);
unsigned int get_rbufsize(int sock);

/**
 *  man socket: (UDP, TCP)
 *  Sets or gets the maximum socket send buffer in bytes.  The default value is set 
 *  by the OS (Linux: rmemdefault, rmem_max sysctl)
 */
int set_sbufsize(unsigned int size, int sock);
unsigned int get_sbufsize(int sock);

/**
 * man tcp:
 * disable the Nagle algorithm.  This means that segments are always sent as soon as
 * possible, even if there is only a small amount of data.  When not set, data  is  buffered
 * until  there is a sufficient amount to send out, thereby avoiding the frequent sending of
 * small packets, which results in poor utilization of the network.
 */
int  set_nodelay(bool nodelay, int sock);
bool get_nodelay(int sock);

/**
 * man socket: (TCP)
 * Indicates  that  the rules used in validating addresses supplied in a bind(2) call should
 * allow reuse of local addresses. For PF_INET sockets this means that a  socket  may  bind,
 * except when there is an active listening socket bound to the address.  When the listening
 * socket is bound to INADDR_ANY with a specific port then it is not  possible  to  bind  to
 * this port for any local address.
 * 
 */
int set_reuseaddr(bool reuse, int sock);
bool get_reuseaddr(int sock);

/**  
 * man tcp: 
 * The number of seconds a connection needs to be idle before TCP begins sending  out  
 * keepalive probes.  Keep-alives are only sent when the SO_KEEPALIVE socket option is 
 * enabled.
 * The default value is 7200 seconds (2 hours).  An  idle  connection  is  terminated  after
 * approximately  an  additional  11 minutes (9 probes an interval of 75 seconds apart) when
 * keepalive is enabled.
 */
int set_keepalive(bool keepalive, int sock);
bool get_keepalive(int sock);

/**
 * man tcp:
 * The maximum segment size for outgoing TCP packets.  If this option is set before  
 * connection establishment, it also changes the MSS value announced to the other end 
 * in the initial packet. Values greater than the (eventual) interface MTU have no effect.  
 * TCP  will also impose its minimum and maximum bounds over the value provided.
 */
int set_mss(unsigned int mss, int sock);
unsigned int get_mss(int sock);

int  set_flabel(unsigned int flabel, struct sockaddr_in6 *addr, int sock);
unsigned int get_flabel(struct sockaddr_in6 addr, int sock);

int  set_cgctrl(char *cgctrl, int sock);

/**
 *  man ipv6: Set Hop Limit
 */
int set_hop_limit(unsigned int hl, int sock);

/****************************
 * IPTOS_RELIABILITY
 * IPTOS_LOWDELAY 
 * IPTOS_MINCOST
 * IPTOS_THROUGHPUT
 ****************************/

/**
 *  man ip: Set Disc/TOS. User with special capabilities needed.
 */
int set_tos_mindelay(int sock);

/**
 *  man ip: Set Disc/TOS. User with special capabilities needed.
 */
int set_tos_maxthput(int sock);

/**
 *  man ip: Don't fragment Flag
 */
//int  set_dontfrag(int sock);
//int  unset_dontfrag(int sock);
//int  get_dontfrag(int sock);

/**
 *  man UDP: Multicast
 * Not Implemented Yet
 */
int  set_mcast(int sock);
bool get_mcast(int sock);

#endif

/**** EOF ****/

