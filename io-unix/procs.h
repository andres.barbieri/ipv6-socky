#include <stdbool.h>

#ifndef _PROC_H
#define _PROCS_H

typedef long long mylong_t;

/**** GENERATORS ****/

mylong_t repchar_delay(int in, int out, int err, 
                       mylong_t times, unsigned int sec, char ch);

mylong_t randchars_delay(int in, int out, int err, 
                         mylong_t times, unsigned int sec);

mylong_t genchars_delay(int in, int out, int err, 
                        mylong_t times, unsigned int sec);

mylong_t time_and_sleep(int in, int out, int err, 
                        mylong_t times, unsigned int sec, bool inchars);

mylong_t genchars_chunks_delay(int in, int out, int err, 
                               mylong_t times, unsigned int sec, unsigned int chunk);

mylong_t genchars_delay_timeout(int in, int out, int err, 
                                mylong_t times, unsigned int sec, int timeout);

mylong_t genchars_chunks_timeout(int in, int out, int err, 
                                 mylong_t times, unsigned int chunk, int timeout);

/**** CONSUMERS ****/

mylong_t hole_and_count(int in, int out, int err, 
                        mylong_t times, unsigned int sec);


mylong_t hole_and_count_chunk(int in, int out, int err, 
                              mylong_t times, unsigned int chunk);

int null(int in, int out, int err);

int null2(int in, int out, int err);

mylong_t echo_delay(int in, int ou, int err, mylong_t times, unsigned int sec);

mylong_t echo_timeout(int in, int out, int err, mylong_t times, int timeout);

/** Both **/
mylong_t in_out(int in1, int in2, int out1, int out2, int err, mylong_t times);

double dwalltime();

#endif

/*** # EOF # ***/   
