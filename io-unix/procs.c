/*****************************
 *
 * FILE NAME:   procs.c
 *
 * $Author:   Andres Barbieri$ barbieri@cespi.unlp.edu.ar
 *
 * $Revision: 0.1.1$
 *
 * $Date:     2015/10/27$
 *
 * Define una serie de funciones
 * para ser aplicadas en diferentes
 * esquemas de comunicación
 *
 *****************************/

#include <stdlib.h>     /** rand, srand **/
#include <unistd.h>     /** write, read, close , sleep **/
#include <time.h>       /** time **/
#include <stdio.h>      /** snprintf  **/
#include <string.h>     /** memset  **/
#include <sys/select.h> /** select **/
#include <sys/time.h>   /** gettimeofday **/
#include <signal.h>     /** signal **/

#ifdef _TIMER_THREAD
#include <time.h>       /** timer_create **/
#endif

#include "procs.h"

/***
 * Parametros Globales (Constantes)
 */

#define LOW        33
#define HIGH      122
#define BUF_SZ     32

#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define MAX_CHUNK 16436

static int tmp_fd   = 0;
int alrm_handler(int sig)
{
  if (tmp_fd > 0) close (tmp_fd);
  tmp_fd = 0;
  return -1;
};

#define CLOCKID CLOCK_REALTIME
#define SIG SIGUSR1  

static void timer_handler(int sig, siginfo_t *si, void *uc)
{
  //fprintf(stderr,"Closing file %d\n",si->si_value.sival_int);fflush(stderr);
  close(si->si_value.sival_int);
}


/*@ unused @*/ static int stop_timer(timer_t timerid)
{
  struct itimerspec its;

  its.it_value.tv_sec = 0;
  its.it_value.tv_nsec = 0;
  its.it_interval.tv_sec = 0;
  its.it_interval.tv_nsec = 0;
  return timer_settime(timerid, 0, &its, NULL);
}

// Alternatives:
// signal(SIGPIPE, SIG_IGN);
// OR
// int n = 1; setsockopt(thesocket, SOL_SOCKET, SO_NOSIGPIPE, &n, sizeof(n));
/*@ unused @*/ void block_sigpipe()
{
  sigset_t sigpipe_mask, old;
  sigemptyset(&sigpipe_mask);
  sigaddset(&sigpipe_mask, SIGPIPE);
  sigprocmask(SIG_BLOCK, &sigpipe_mask, &old);
}


/**** GENERATORS ****/

mylong_t repchar_delay(int in, int out, int err, 
                       mylong_t times, unsigned int sec, char ch)
{
  mylong_t i;
  mylong_t count = 0;
  int      c     = ch;
  int      rval;
  // Genera chars repetidos 
  for(i=0;(i<times)||(times==-1);i++)
    {
      sleep(sec);
      rval = write(out, &c, sizeof(char));
      if (rval <= 0) { /*write error*/ return count; }
      else { /*OK*/ count+=rval; }
    }
  return count;
}

mylong_t randchars_delay(int in, int out, int err, 
                         mylong_t times, unsigned int sec)
{
  mylong_t i;
  mylong_t count = 0;
  int      rval;
  int      c;
 
  // Agrega algo de entrophy
  unsigned int seed = (unsigned int) time(NULL);
  srand(seed); 
  // Genera chars aleatorios
  for(i=0;(i<times)||(times==-1);i++)
  {
    // RAND_MAX MUST BE != 0
      c =  LOW + (int) ( ((float) rand() / RAND_MAX ) * (HIGH - LOW) );
      sleep(sec);
      rval = write(out, &c, sizeof(char));
      if (rval <= 0) { /*write error*/ return count; }
      else { /*OK*/ count+=rval; }
  }
  return count;
}

mylong_t genchars_delay(int in, int out, int err, 
                        mylong_t times, unsigned int sec)
{
  mylong_t i;
  mylong_t count = 0;
  int      c;
  int      rval;
  // Genera chars en secuencia
   for(i=0;(i<times)||(times==-1);i++)
    {
      c = (i % 94) + 33;
      sleep(sec);
      rval = write(out, &c, sizeof(char));
      if (rval <= 0) { /*write error*/ return count; }
      else { /*OK*/ count+=rval; }
    }
  return count;
}

mylong_t time_and_sleep(int in, int out, int err, 
                        mylong_t times, unsigned int sec, bool inchars)
{
  mylong_t     i;
  double       now;
  int          rval;
  char         buf[BUF_SZ];

  for(i=0;(i<times)||(times==-1);i++)
    {
      now = dwalltime();
      if (inchars)
	{
	  memset(buf, ' ', BUF_SZ);
	  snprintf(buf,BUF_SZ,"%f",now);
	  rval = write(out, buf, BUF_SZ);
	}
      else
	{
	  rval = write(out, &now, sizeof(double));
	}
      if (rval <= 0)
	return (mylong_t) rval;
      sleep(sec);
    }
  return 0;
}

static int  write_chunk(int fd, char tmp[], int size)
{
  int sent = 0;
  int rval = 1;

  while ( (sent<size)&&(rval>0) )
    {
      rval = write(fd, tmp, (size-sent));
      if (rval>0)
	{
	  sent+=rval;
	  tmp+=rval;
	}
      else
	{
	  sent = rval;
	}
    }
  return sent;
}

mylong_t genchars_chunks_delay(int in, int out, int err, 
                               mylong_t times, unsigned int sec, 
                               unsigned int chunk)
{
  mylong_t i;
  mylong_t count = 0;
  int      rval;
  char     tmp[min(chunk,MAX_CHUNK)];
  
  for(i=0;i<chunk;i++)
    {
      tmp[i] = (i % 94) + 33;
    }
  // chunk MUST BE > 0 - Floating point exception
  if (chunk<=0) chunk = 1;
  for(i=0;(i<(times/chunk))||(times==-1);i++)
    {
      // Escribe de a chunks debe controlar que escribio 
      // lo indicado, puede suceder rval<sizeof(tmp)
      sleep(sec);
      rval = write_chunk(out, tmp, sizeof(tmp));
      if (rval <= 0) { /*write error*/ return count; }
      else { /*OK*/ count+=(long long)rval; }
    }
  for(i=0;i<(times%chunk);i++)
    {
      // Escribe de a una unidad
      rval = write(out, &(tmp[i]), sizeof(char));
     if (rval <= 0)
	{
	  return count;
	}
      else
	{
	  count+=(long long)rval;
	}
    }
  return count;
}

/*** Pre-cond : sec>0 , sino no funciona el timeout ***/
static mylong_t genchars_delay_timeout_aux(int in, int out, int err, 
                                           mylong_t times, unsigned int sec, 
                                           int timeout)
{
  mylong_t i;
  mylong_t count = 0;
  int      c;
  int      rval;
  fd_set   set;
  struct timeval tout;

  /* Initializa struct timeout, solo al init  */
  tout.tv_sec = timeout;
  tout.tv_usec = 0;
  for(i=0;(i<times)||(times==-1);i++)
    {
      /* Initializa descriptor set cada vez */
      FD_ZERO (&set);
      FD_SET (out, &set);
      c = (i % 94) + 33;
      sleep(sec);
      if ((rval = select (FD_SETSIZE, NULL, &set, NULL, &tout))>0)       
	{ /*Can write*/
	  if ((tout.tv_sec - sec) >= 0) { tout.tv_sec-=sec; }
	  else { /*tmout*/ return count; }	    
	  rval = write(out, &c, sizeof(char));
	  if (rval <= 0) { /*write error*/ return count; }
	  else { /*OK*/ count+=rval; }
	}
      else 
	{ 
          /* select error or tmout */
	  return count;
	}
  }
  return count;
}

static mylong_t genchars_timeout_aux(int in, int out, int err, 
                                     mylong_t times, int timeout)
{
  mylong_t i;
  mylong_t count = 0;
  int      c;
  int      rval;

#ifndef _TIMER_THREAD
  alarm(timeout);
  signal(SIGALRM, (__sighandler_t) alrm_handler);
  tmp_fd = out;
  //#if ( (defined(SUN4) || defined(LINUX)) 
  siginterrupt(SIGALRM, 1);
  //#endif
#else
  struct sigevent sev;
  struct itimerspec its;
  struct sigaction sa;
  timer_t timerid;
  
  sa.sa_flags = SA_SIGINFO;
  sa.sa_sigaction = timer_handler;
  sigemptyset(&sa.sa_mask);
  sigaction(SIG, &sa, NULL);
  sev.sigev_notify = SIGEV_SIGNAL;
  sev.sigev_signo = SIG;
  //sev.sigev_value.sival_ptr = &timerid;
  /* File Descriptor a cerrar */
  sev.sigev_value.sival_int = out;
  timer_create(CLOCKID, &sev, &timerid);
  /* Set the timer */
  its.it_value.tv_sec = timeout;
  its.it_value.tv_nsec = 0;
  its.it_interval.tv_sec = its.it_value.tv_sec;
  its.it_interval.tv_nsec = its.it_value.tv_nsec;
  /* Start the timer */
  timer_settime(timerid, 0, &its, NULL);
#endif
  for(i=0;(i<times)||(times==-1);i++)
    {
      c = (i % 94) + 33;
      rval = write(out, &c, sizeof(char));
      if (rval <= 0) 
	{ /*write error, may be interrupted*/  
#ifndef _TIMER_THREAD
	  alarm(0); 
#else
	  /* Stop and delete the timer */
	  timer_delete(timerid);
#endif
	  return count; }
      else 
	{ /*OK*/ 
	  count+=rval; 
	}
    }
#ifndef _TIMER_THREAD
 alarm(0);
#else
 /* Stop and delete the timer */
 timer_delete(timerid);
#endif
 return count;
}

mylong_t genchars_delay_timeout(int in, int out, int err, 
                                mylong_t times, unsigned int sec, 
                                int timeout)
{
  if (sec>0)
    return genchars_delay_timeout_aux(in, out, err, times, sec, timeout);
  else
    return genchars_timeout_aux(in, out, err, times, timeout);
}

mylong_t genchars_chunks_timeout(int in, int out, int err, 
                                 mylong_t times, unsigned int chunk, int timeout)
{
  mylong_t i;
  mylong_t count = 0;
  int      rval;
  char     tmp[min(chunk,MAX_CHUNK)];
  
  for(i=0;i<chunk;i++)
    {
      tmp[i] = (i % 94) + 33;
    }
#ifndef _TIMER_THREAD
  alarm(timeout);
  signal(SIGALRM, (__sighandler_t) alrm_handler);
  tmp_fd = out;
  //#if ( (defined(SUN4) || defined(LINUX)) 
  siginterrupt(SIGALRM, 1);
  //#endif
#else
  struct sigevent sev;
  struct itimerspec its;
  struct sigaction sa;
  timer_t timerid;
  
  sa.sa_flags = SA_SIGINFO;
  sa.sa_sigaction = timer_handler;
  sigemptyset(&sa.sa_mask);
  sigaction(SIG, &sa, NULL);
  sev.sigev_notify = SIGEV_SIGNAL;
  sev.sigev_signo = SIG;
  //sev.sigev_value.sival_ptr = &timerid;
  /* File Descriptor a cerrar */
  sev.sigev_value.sival_int = out;
  timer_create(CLOCKID, &sev, &timerid);
  /* Set the timer */
  its.it_value.tv_sec = timeout;
  its.it_value.tv_nsec = 0;
  its.it_interval.tv_sec = its.it_value.tv_sec;
  its.it_interval.tv_nsec = its.it_value.tv_nsec;
  /* Start the timer */
  timer_settime(timerid, 0, &its, NULL);
#endif

  // chunk MUST BE > 0 - Floating point exception
  if (chunk<=0) chunk = 1;
  for(i=0;(i<(times/chunk))||(times==-1);i++)
    {
      // Escribe de a chunks debe controlar que escribio 
      // lo indicado, puede sicder rval<sizeof(tmp)
      rval = write_chunk(out, tmp, sizeof(tmp));
      if (rval <= 0) 
	{ /*write error, may be interrupted*/  
#ifndef _TIMER_THREAD
	  alarm(0); 
#else
	  /* Stop and delete the timer */
	  timer_delete(timerid);
#endif
	  return count; }
      else 
	{ /*OK*/ 
	  count+=rval; 
	}
    }
  /* Ultimo chunk parcial */
  for(i=0;i<(times%chunk);i++)
    {
      // Escribe de a una unidad
      rval = write(out, &(tmp[i]), sizeof(char));
     if (rval <= 0)
	{ /*write error, may be interrupted*/  
#ifndef _TIMER_THREAD
	  alarm(0); 
#else
	  /* Stop and delete the timer */
	  timer_delete(timerid);
#endif
	  return count;
	}
      else
	{
	  count+=rval;
	}
    }
#ifndef _TIMER_THREAD
  alarm(0);
#else
  /* Stop and delete the timer */
  timer_delete(timerid);
#endif
  return count;
}

/**** CONSUMERS ****/

mylong_t hole_and_count(int in, int out, int err, 
                        mylong_t times, unsigned int sec)
{
  mylong_t count = 0;
  int      i;
  int      c;

  while ( ( ( i = read(in , &c, sizeof(c)) ) > 0 ) && 
          ( (count < times)||(times==-1) )  )   
    {
      count+=i;
      sleep(sec);
    }
  return count;      
}

mylong_t hole_and_count_chunk(int in, int out, int err, 
                              mylong_t times, unsigned int chunk)
{
  mylong_t count = 0;
  int      i;
  char     tmp[min(chunk,MAX_CHUNK)];

  while ( ( ( i = read(in , tmp, sizeof(tmp)) ) > 0 ) && 
           ( (count < times)||(times==-1) )  )
    {
      count+=i;
    }
  return count;      
}

int null(int in, int out, int err)
{
  return close(in);
}

int null2(int in, int out, int err)
{
  return ( close(in) && close(out) );
}

mylong_t echo_delay(int in, int out, int err, 
                    mylong_t times, unsigned int sec)
{
  mylong_t count = 0;
  int      rval;
  int      c;

  while ( ( read(in, &c, sizeof(char)) > 0 ) &&
	  ( (count < times)||(times==-1) )  )
    {
      sleep(sec);
      rval = write(out , &c, sizeof(char) );
      if (rval>0)
	count+=rval;
      else
	break;
    }
  return count;
}

mylong_t echo_timeout(int in, int out, int err, 
                      mylong_t times, int timeout)
{
  mylong_t count = 0;
  int      rval;
  int      c;

#ifndef _THREAD_TIMER
  alarm(timeout);
  signal(SIGALRM, (__sighandler_t) alrm_handler);
  // #if defined(SUN4) || defined(LINUX)
  siginterrupt(SIGALRM, 1);
  //#endif
#else
  struct sigevent sev;
  struct itimerspec its;
  struct sigaction sa;
  timer_t timerid;
  
  struct sigevent sev;
  struct itimerspec its;
  struct sigaction sa;
  timer_t timerid;
  
  sa.sa_flags = SA_SIGINFO;
  sa.sa_sigaction = timer_handler;
  sigemptyset(&sa.sa_mask);
  sigaction(SIG, &sa, NULL);
  sev.sigev_notify = SIGEV_SIGNAL;
  sev.sigev_signo = SIG;
  //sev.sigev_value.sival_ptr = &timerid;
  /* File Descriptor to close on signal */
  sev.sigev_value.sival_int = in;
  timer_create(CLOCKID, &sev, &timerid);
  /* Set the timer */
  its.it_value.tv_sec = timeout;
  its.it_value.tv_nsec = 0;
  its.it_interval.tv_sec = its.it_value.tv_sec;
  its.it_interval.tv_nsec = its.it_value.tv_nsec;
  /* Start the timer */
  timer_settime(timerid, 0, &its, NULL);
 
#endif
    while ( ( read(in, &c, sizeof(char))  > 0 )&& ( (count<times)||(times==-1) ) )
    {
      rval = write(out , &c, sizeof(char) );
      if (rval>0)
	count+=rval;
      else
	break;
    }
#ifndef _THREAD_TIMER
  alarm(0);
#else
  /* Stop and delete the timer */
  timer_delete(timerid);
#endif
  return count;
}

mylong_t in_out(int in1, int in2, int out1, int out2, int err, mylong_t times)
{
  mylong_t i;
  mylong_t count = 0;
  int      c;
  int      rval;
  fd_set   set_r;
  int      in_var  = in1;
  int      out_var = out1;

  /* Initialize the timeout data structure */
  for(i=0;(i<times)||(times==-1);i++)
    {
      /* Initialize the file descriptor set */
      FD_ZERO (&set_r);
      FD_SET (in1, &set_r);
      FD_SET (in2, &set_r);
      if ((rval = select (max(in1,in2)+1, &set_r, NULL, NULL, NULL))>0)       
	{ /*Can read*/
	  /*See which is ready*/
	  if (FD_ISSET(in1, &set_r))
	    {
	      in_var  = in1;
	      out_var = out1;
	    }
	  else if (FD_ISSET(in2, &set_r))
	    {
	      in_var  = in2;
	      out_var = out2;
	    }	   
	  else
	    {
	      /* Should be 0 return value */
	      return count;
	    }
	  rval = read(in_var, &c, sizeof(char));
	  if (rval <= 0) { /*read error*/ return count; }
	  rval = write(out_var, &c, sizeof(char));
	  //printf("wrote %c to %d\n",c,out_var);fflush(stdout);
	  if (rval <= 0) { /*write error*/ return count; }
	  else { /*OK*/ count+=rval; }
	}
      else 
	{ 
	  /* select error */
	  return count;
	}
    }
  /*Exit on success*/
  return count;
}

double dwalltime()
{
        double usec;
        struct timeval tv;
 
        if (gettimeofday(&tv,NULL) < 0)
          {
            return -1;
          }
        usec = (tv.tv_sec * 1000000.0) + tv.tv_usec;
        return usec;
}

/*** # EOF # ***/   
