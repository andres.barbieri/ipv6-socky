/***
 * $Author: Andres Barbieri$ barbieri@cespi.unlp.edu.ar
 * $Date: 2006/05/27$
 * $Revision: 0.1$
 *
 * Interfaz para los server a ser corridos con tcp-socky.c
 *
 */
#define _SERVER_H

typedef int server_err_t;

#define SERVER_OK        0
#define SERVER_ERRWRITE  1
#define SERVER_ERRREAD   2
#define SERVER_ERRERR    3
#define SERVER_ERRARG    4
#define SERVER_ERRMEM    5

server_err_t do_service(int fd, int argc, char *argv[]);

void server_error(server_err_t err);

/*** # EOF # ***/
