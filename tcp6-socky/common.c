#include <stdlib.h>  /** malloc, free, exit **/
#include <unistd.h>  /** write, close **/
#include <stdio.h>   /** perror, flush **/
#include <stdbool.h> /** Booleans **/ 
#include <signal.h>  /** signal **/
#include <procs.h>
#include "common.h"

/***
 * Ejecuta Something: Client or server
 */
int  do_sth(int fd, 
            int argc, 
            /*@ in @*/ char *argv[])
{
  mylong_t     rval;
  FILE         *myerr  = NULL;
  double       start_time, end_time;

  char         opt    = '+';
  mylong_t     times  = 0;
  unsigned int delay  = 0;
  unsigned int chunk  = 8192;
  char         rchar  = 'A';
  int          tmout  = 0;
  

  /***
   * Convierte el FD en FP para generar errores
   */  
  myerr = fdopen(2, "a");
  if ( myerr == NULL )
	   return -1;

  /***
   * Testea los Argumentos
   */  

  /***
  argv[0] == --extra|-E
  argv[1] == '+'
             'h' [times] [delay]
             'k' [times] [chunk]
             'e' [times] [delay] [tmout] 
             'p' [times] [delay] [tmout] 
             'C' [times] [delay] [tmout] 
             'A' [times] [delay] [rchar] 
             'R' [times] [delay]
             'K' [times] [chunk] [tmout]
             'T' [times] [delay]
   argv[2] == times
   argv[3] == delay|chunk
   argv[4] == tmout|rchar
  ***/
  // +2 args
  if (argc >= 2)
    {
      opt = argv[1][0];
    } 
  // +3 args 
  if (argc >= 3)
    {
      times = (mylong_t) atoll(argv[2]);
      //printf("%lld %d %d\n",times,sizeof(long long),sizeof(mylong_t));
      //exit(1);
    }
  // +4 args
  if ( (argc >= 4) && ((argv[1][0]=='h')||(argv[1][0]=='e')||(argv[1][0]=='p')||(argv[1][0]=='C')||
                       (argv[1][0]=='A')||(argv[1][0]=='R')|| (argv[1][0]=='T')) )
    {
      delay = atoi(argv[3]);
    } 
  if ( (argc >= 4) && ((argv[1][0]=='K')||(argv[1][0]=='k'))  )
    {
      chunk = atoi(argv[3]);
    }  
  // +5 args
  if ( (argc >= 5)&&((argv[1][0]=='A')) )
    {
      rchar = argv[4][0];
    }  
  if ( (argc >= 5)&&((argv[1][0]=='e')||(argv[1][0]=='p')||(argv[1][0]=='C')||(argv[1][0]=='K')) )
    {
      tmout = atoi(argv[4]);
    }  
 printf("Extra Options: opt=%c times=%lld delay=%d chunk=%d rchar=%c tmout=%d\n",
	opt,times,delay,chunk,rchar,tmout);
 if (times == 0) times = -1; 
  start_time = dwalltime();
  switch (opt)
    {
   case '+':      
     rval = in_out(0, fd, fd, 1, 2, times);
     break;
    case 'h':      
      rval = hole_and_count(fd, 1, 2, times, delay);
      break;
    case 'k':
      rval = hole_and_count_chunk(fd, 1, 2, times, chunk);
      break;
    case 'e':
      if (tmout <= 0) { rval = echo_delay(fd, fd, 2, times, delay); }
      else { rval = echo_timeout(fd, fd, 2, times, tmout); }
      break;       
    case 'p':
      if (tmout <= 0) { rval = echo_delay(fd, 1, 2, times, delay); }
      else { rval = echo_timeout(fd, 1, 2, times, tmout); }
      break;        
    case 'R':
      rval = randchars_delay(0, fd, 2, times, delay);
      break;
    case 'C':
      if (tmout <= 0) { rval = genchars_delay(0, fd, 2, times, delay); }
      else { rval = genchars_delay_timeout(0, fd, 2, times, delay, tmout); }
      break;
    case 'A':
      rval = repchar_delay(0, fd, 2, times, delay, rchar);
      break;
    case 'K':
      if (tmout <= 0)
	{ rval = genchars_chunks_delay(0, fd, 2, times, delay, chunk); }
      else { rval = genchars_chunks_timeout(0, fd, 2, times, chunk, tmout); }
      break;
    case 'T':
      rval = time_and_sleep(0, fd, 2, times, delay, true);
    break;
    default:
      printf("Bad option using default\n");
      rval = in_out(0, fd, 1, fd, 2, times);
    }
  end_time = dwalltime();
  printf("\nproc %lld bytes in ",(long long int) rval);
  printf("%f usec or %f sec\n",(end_time - start_time), (end_time-start_time)/1000000);
  printf("BW=%f Mbps\n", 
	 (rval*8)/ ((end_time-start_time)/1000000) / 1000000 ); 

  /** Finaliza la Instancia **/
  close(fd);
  return (0);
}

/*** # EOF # ***/
