#include <stdlib.h>  /** malloc, free, exit **/
#include <unistd.h>  /** write, close **/
#include <stdio.h>   /** perror, flush **/
#include <stdbool.h> /** Booleans **/ 

#include "common.h"
#include "server.h"

/***
 * NAIVE Server Error Handler
 */
void server_error(server_err_t err)
{
  printf("Error at server side %d\n",err);
}


/***
 * Ejecuta el Servicio
 */
server_err_t  do_service(int fd, 
                         int argc, 
                         /*@ in @*/ char *argv[])
{
  if (do_sth(fd, argc, argv) == 0)
    return SERVER_OK;
  else
    return -SERVER_ERRARG;


}

/*** # EOF # ***/
