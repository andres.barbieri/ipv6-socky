/**   
 *------------------------------------------------------------------------
 *
 * FILE NAME:   tcp6-socky.c
 *
 * PURPOSE:     
 *
 * Para tesis del Magister de Redes
 *
 * FILE REFERENCES:        
 *
 *            Name               I/O       Description             
 *            ----               ---       -----------
 * @file      <fname>            <i|o|io>  <fdesc>
 * 
 * EXTERNAL REFERENCES:
 *
 *            Name               I/O       Description             
 *            ----               ---       -----------
 * @external  <eref>             <i|o|io>  <edesc> 
 *
 * MAIN: 
 *
 * $Author:   Andres Barbieri$ barbieri@cespi.unlp.edu.ar
 *
 * $Revision: 0.1$
 *
 * $Date:     2014/06/21$
 *
 * REQUIREMENTS/FUNCTIONAL SPECIFICATIONS:
 *
 * Ver par�metros de una comunicaci�n TCP (RFC793).  
 *
 * NOTES:
 *
 * Para correr ver notas m�s abajo (A�n no se agreg�).
 *
 * Utiliza las llamadas est�ndares de SOCKET BSD.
 * All functions: socket(), accept(), connect(), listen() calls
 * conforms 4.4BSD, SUSv2, POSIX 1003.1-2001.  
 * These function calls appeared in 4.2BSD.
 *
 * DEVELOPMENT HISTORY:
 *
 * Date        Author     Revision    Release   Description  
 * ----        ------     --------    -------   -----------
 * 2006/04/01  andres          0.1       -      Start
 *
 *------------------------------------------------------------------------
 */
 
#include <unistd.h>                     /* close , read , fork, sleep, write */
#include <sys/types.h>                  /* socket, connect */
#include <sys/socket.h>                 /* socket , connect*/
#include <arpa/inet.h>                  /* ip socket , sockaddr_in6 inet_ntop */
#include <stdlib.h>                     /* exit */
#include <stdio.h>                      /* printf , stderr, perror*/
#include <errno.h>                      /* errno  */
#include <stdbool.h>                    /* Booleans */
#include <string.h>                     /* strerror_r */
#include <pthread.h>                    /* pthread */
#include <signal.h>                     /* signal */
#include <sys/wait.h>                   /* wait */

#include <options.h>                    /* set_<OPT> */
#include <procs.h>                      /* dwalltime() */

#include "parse_arguments.h"            /* parse_arguments */
#include "server.h"                     /* do_service(in, out, err, argc, argv) */
#include "client.h"                     /* do_client(in, out, err, argc, argv)  */

/*********************************************************
 * Handle Error Messages.
 * Manejador de errores, mostrador, reentrant mode
 */
#ifndef _MYPRERROR_C
#include <myperror.c>
#endif

/*********************************************************
 * Hace un delay y muestra cuanto espera
 * 
 */

static void show_sleep(int delay)
{
  int i;

  if (delay == 0) 
    return;
  fprintf(stderr,"Sleeping for %d sec ",delay);
  fflush(stdout);
  for(i=0;i<delay;i++)
    {
      sleep(1);
      printf(".");
      fflush(stdout);
    }
  printf("\n");
}

/*********************************************************
 * Muestra las opciones del socket
 *
 */
static void show_options(int sock, struct sockaddr_in6 addr)
{
   fprintf(stderr,"RBUF:      %d\n",get_rbufsize(sock));
   fprintf(stderr,"SBUF:      %d\n",get_sbufsize(sock));
   fprintf(stderr,"MSS:       %d\n",get_mss(sock));
   fprintf(stderr,"REUSE:     %d\n",get_reuseaddr(sock));
   fprintf(stderr,"NODELAY:   %d\n",get_nodelay(sock));
   fprintf(stderr,"KEEPALIVE: %d\n",get_keepalive(sock));
}

/*********************************************************
 * Instancia del proceso servidor
 *
 */

static void server_proc(int fd, int argc, char *argv[])
{
  server_err_t res;
  
  res = do_service(fd, argc, argv);
  fprintf(stderr," Return: %d\n",res);
  fflush(stderr);
  if (res == -SERVER_ERRWRITE)
    {
      my_perror_r("ERROR: at write to tcp socket");
    }
  else if (res < 0)
    {
      server_error(res);
      exit(EXIT_FAILURE);
    }
}

/*********************************************************
 * Instancia del proceso cliente
 *
 */
static void client_proc(int fd, int argc, char *argv[])
{
  client_err_t res;
  
  res = do_client(fd, argc, argv);
  fprintf(stderr," Return: %d\n",res);
  fflush(stderr);	      
  if (res < 0)
    {
      client_error(res);
      exit(EXIT_FAILURE);
    }
}

/*********************************************************
 * Variables globales usadas para
 * la threads' communication
 */
volatile int          glob_wait;

static   int          glob_fd;
static   int          glob_argc; 
static   char         **glob_argv;

/*********************************************************
 * Usado para servir los request
 * en modo thread
 */
void* server_thread(void *arg)
{
 // Copy Global variables 
 int  fd        = glob_fd;
 int  argc      = glob_argc;
 char **argv    = glob_argv;

 // Signal (WakeUp) threads creator
 glob_wait      = 1;  
 // Ejectua server thread
 server_proc(fd, argc, argv);
}

/*********************************************************
 * Genera un nuevo proceso
 *
 */
static void fork_server_proc(int fd, int argc, char *argv[])
{
  pid_t pid;

  pid = fork();
  if (pid == 0)
    {
      server_proc(fd, argc, argv);
      exit(EXIT_SUCCESS);
    }
  else if (pid > 0)
    {
      close(fd);
    }
  else
    {
      my_perror_r("ERROR: at forking new process");
      exit(EXIT_FAILURE);
    }
}

/*********************************************************
 * Genera un nuevo thread
 *
 */
static void thread_server_proc(int fd, int argc, char *argv[])
{
 pthread_t t1; 
 int       arg = 0;
 
 // Set Global Variables
 glob_fd        = fd;
 glob_argc      = argc;
 glob_argv      = argv;

 glob_wait      = 0;
 // Create thread
 if (pthread_create(&t1, NULL, server_thread, &arg) != 0)
 {
   my_perror_r("ERROR: at creating new thread");
   exit(EXIT_FAILURE);
 } 
 // Busy Wait until thread copy global variables
 while (!glob_wait);
}

/*********************************************************
 * Junta/colecta los children terminados
 *
 */

static int finished = 0;

void chld_signal_handler (int signal)
{
  if (signal == SIGCHLD) 
    {
      int   status;
      pid_t pid;
      while ((pid = waitpid (-1, &status, WNOHANG)) > 0)
	{
	  printf("%d return with %d\n",pid, status);
	  finished++;
	}
    }
  return;
}

void pipe_signal_handler (int signal)
{
  if (signal == SIGPIPE) 
    {
      printf("Broken PIPE");
      fflush(stdout);
    }
  return;
}

/************************
 *
 * MAIN PROGRAM
 *
 ************************/

int main(int argc, char *argv[])
{
  // Socket and Address Structs
  int                 sock; 
  struct sockaddr_in6 myaddr;
  struct sockaddr_in6 destaddr;
  struct sockaddr_in6 cliaddr;
  // Program Parameters
  char               *local     = NULL;
  int                port       =  0;
  int                delay      =  0;
  int                recvbuf    =  0;     
  int                sendbuf    =  0;     
  int                mss        =  0;     
  bool               reuse      = false;
  bool               nodelay    = false;
  bool               keepalive  = false;
  int                blog       =  0;
  char               *dst       = NULL;
  int                sport      =  0;
  int                forking    =  0;
  bool               lismode    = false;
  unsigned int       flabel     =  0;
  char               *cgctrl    = NULL;
  // Tmp space
  char               tmp[64];
  
  /***
   * Parsea los argumentos
   */
  if ( parse_arguments(argc, argv, &local, &port, &delay, &recvbuf, &sendbuf, &mss,
		       &reuse, &nodelay, &keepalive, &blog, &forking, &dst, &sport, 
                       &cgctrl, &flabel, &lismode) 
                       != 0 )
    {
      usage(argv[0], EXIT_FAILURE);
    }
  /***
   *  Crea el INET6 Domain/TCP socket       
   */
  //sock = socket(AF_INET, SOCK_STREAM, 0);
  if ((sock = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
      my_perror_r("ERROR: at opening tcp6 socket");
      fflush(stderr);
      exit(EXIT_FAILURE);
    }
  /***
   * Setea las opciones comunes requeridas
   * On fail no hace exit
   */
  set_rbufsize(recvbuf, sock);
  set_sbufsize(sendbuf, sock);
  set_mss(mss, sock);
  if (reuse) 
    {	 
      set_reuseaddr(reuse, sock);
    }
  if (nodelay)
    {	 
      set_nodelay(nodelay, sock);
    }
  if (keepalive)
    {	 
      set_keepalive(keepalive, sock);
    }
  /***
   *  Hace el bind
   */
  //myaddr.sin_family      = AF_INET;
  //myaddr.sin_addr.s_addr = inet_addr(local); //htonl(INADDR_ANY);
  /* Set up for IPv6 */
  myaddr.sin6_family       = AF_INET6;
  myaddr.sin6_flowinfo     = 0x0;
  if (inet_pton(AF_INET6, local, &(myaddr.sin6_addr) ) <= 0)
    {
      perror("ERROR: at translating address");
      fflush(stderr);
      exit(EXIT_FAILURE);
    }
  if (lismode)
    {
      myaddr.sin6_port        = htons((uint16_t) port);
    }
  else
    {
      myaddr.sin6_port        = htons((uint16_t) sport);
    }
  if ( bind(sock, (struct sockaddr *)&myaddr,sizeof(struct sockaddr_in6)) < 0 ) 
    {
      my_perror_r("ERROR: at binding to stream tcp6 socket");
      fflush(stderr);
      close(sock);
      exit(EXIT_FAILURE);
    }
  /***
   * Muestra las opciones del socket 
   * Si hubo errores se ven aqui
   */
  show_options(sock, myaddr);
  /***
   * Mueve el puntero a los argumentos hasta los Extra
   */

  parse_until_extra_arguments(&argc, &argv);
 
  /*** Modo server o client ***/
  if (lismode)
    {
      /***
       * Setea el top de la backlog queue y avisa a TCP que accepte conexiones
       */ 
      if ( listen(sock, blog) != 0 )
	{
	  my_perror_r("ERROR: at listen on stream tcp socket");
	  fflush(stderr);
	  close(sock);
	  exit(EXIT_FAILURE);
	}

      /***
       * Configura el handler de los Children finalizados
       */
      signal(SIGCHLD, chld_signal_handler);
      signal(SIGPIPE, pipe_signal_handler);      
      /***
       * Loop de Servicio en modo sever
       */
      while ( true )
	{
	  int                peer_sock = -1; 
	  int                len = sizeof(cliaddr);         
	  struct sockaddr_in6 cliaddr;
	  
	  /***
	   * Hace una demora antes de dar la conexion
	   */
	  show_sleep(delay); 
	  
	  /***
	   * Toma la conexion desde el Stack TCP o Espera que entre una (BLOCK)
	   */
	  fprintf(stderr,"Waiting connection on %s:%d backlog: %d model: %d\n",
                  local, port, blog , forking);
	  peer_sock = accept(sock, (struct sockaddr*) &cliaddr, (socklen_t *) &len);
	  if ( peer_sock < 0 )
	    {
	      if ( errno != EINTR ) 
		{
		  my_perror_r("ERROR: at accepting incoming tcp connection");
		  fflush(stderr);
		  close(sock);
		  exit(EXIT_FAILURE);
		}
	      else
		{
		  // Structural Programming ??, naaaahhh 
		  // Que poder de expresividad !!!!!
		  continue;
		}
	    }
	  
	  /***
	   * Brinda el servicio
	   */
	  //fprintf(stderr,"Connected from %s:%d\n",
	  //	    inet_ntoa(cliaddr.sin_addr),ntohs(cliaddr.sin_port));
	  inet_ntop(AF_INET6, (const void*)&(cliaddr.sin6_addr), tmp, sizeof(tmp));
	  fprintf(stderr,"Connected from %s:%d\n", tmp, (int) ntohs(cliaddr.sin6_port));
	  if ( forking == 1 )
	    {
	      fork_server_proc(peer_sock, argc, argv);
	    }
	  else if (forking == 2 )
	    {
	      thread_server_proc(peer_sock, argc, argv);
	    }
	  else
	    {
	      server_proc(peer_sock, argc, argv);		    
	    }
	}
    }
  else
    {  
      /***
       * Connecta al Servidor. Modo cliente
       */
      //destaddr.sin_family      = AF_INET;
      //destaddr.sin_port        = htons(port);
      //destaddr.sin_addr.s_addr = inet_addr(dst);
      destaddr.sin6_family       = AF_INET6;
      destaddr.sin6_port         =  htons((uint16_t) port);
      if (inet_pton(AF_INET6, dst, &(destaddr.sin6_addr) ) <= 0)
	{
	  perror("ERROR: at translating address");
	  fflush(stderr);
	  exit(EXIT_FAILURE);
	}    
      //fprintf(stderr,"Trying to connect to %s:%d\n",
      //        inet_ntoa(destaddr.sin_addr),ntohs(destaddr.sin_port));
      inet_ntop(AF_INET6, (const void*)&(destaddr.sin6_addr), tmp, sizeof(tmp));
      fprintf(stderr,"Trying to connect to %s:%d\n", tmp, (int) ntohs(destaddr.sin6_port));
      if( flabel != 0)
	{
          if (set_flabel(flabel, &destaddr, sock) == -1)
	    {
	      perror("ERROR: at setting flow label, using default, not exiting ...");
	      fflush(stderr);
	    }
	} 
      else
	{
           destaddr.sin6_flowinfo = 0x0;
	}
      if( cgctrl != NULL)
	{
          if (set_cgctrl(cgctrl, sock) == -1)
	    {
	      perror("ERROR: at setting congestion control algorithm, using current, not exiting ...");
	      fflush(stderr);
	    }
	  else
	    {
	      fprintf(stderr,"TCP Congestion Control Algorithm:%s\n",cgctrl);
	    }
	}
      if (connect(sock, (struct sockaddr *) &destaddr,
		  sizeof(struct sockaddr_in6))<0) 
	{
	  close(sock);
	  perror("ERROR: at connecting to tcp socket"); 
	  fflush(stderr);
	  exit(EXIT_FAILURE);
	} 
      //fprintf(stderr,"Connected to %s:%d\n",
      //        inet_ntoa(destaddr.sin_addr),ntohs(destaddr.sin_port));
      fprintf(stderr,"Connected to %s:%d\n", tmp, (int) ntohs(destaddr.sin6_port));
      show_options(sock, destaddr);
      /***
       * Hace una demora antes de trabajar sobre la conexion
       */
      show_sleep(delay); 
      client_proc(sock, argc, argv);		    
    }
  return (0);
}

/*** # EOF # ***/
