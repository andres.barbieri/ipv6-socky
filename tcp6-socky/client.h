/***
 * $Author: Andres Barbieri$ barbieri@cespi.unlp.edu.ar
 * $Date: 2006/05/27$
 * $Revision: 0.1$
 *
 * Interfaz para los clientes a ser corridos con tcp-socky.c
 *
 */

#define _CLIENT_H

typedef int client_err_t;

#define CLIENT_OK        0
#define CLIENT_ERRWRITE  1
#define CLIENT_ERRREAD   2
#define CLIENT_ERRERR    3
#define CLIENT_ERRARG    4
#define CLIENT_ERRMEM    5

client_err_t do_client(int fd, int argc, char *argv[]);

void client_error( client_err_t err);

/*** # EOF # ***/
